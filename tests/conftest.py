"""
test suit for the automation test cases
"""
import os
import pytest

from libs.utils.public_api import PublicApi


@pytest.fixture(scope="session")
def testbed():
    """
    Retrieve configurations from environment
    :return: env map
    """
    env = dict()

    # Kafka Topics
    # env['verdict_topic'] = os.getenv("VERDICT_TOPIC", 'local-alertVerdict')
    env['verdict_topic'] = os.getenv("VERDICT_TOPIC", 'app14_qa - alertVerdict')
    # env['notification_topic'] = os.getenv("NOTIFICATION_TOPIC", 'local-alertNotification')
    env['notification_topic'] = os.getenv("NOTIFICATION_TOPIC", 'app14_qa-alertChanged')
    env['policy_change_topic'] = os.getenv("POLICY_TOPIC", 'app14_qa_policy_changed')
    env['alertrule_change_topic'] = os.getenv("ALERTRULE_TOPIC", 'app14_qa_alertrule_changed')
    env['account_change_topic'] = os.getenv("ACCOUNT_TOPIC", 'app14_qa_account_changed')
    env['alert_change_topic'] = os.getenv("ALERT_TOPIC", 'app14_qa-alertChanged')

    # connection information
    # env['kafka_servers'] = os.getenv("KAFKA_SERVERS", 'kafka:9093').split()
    env['kafka_servers'] = os.getenv("KAFKA_SERVERS", 'kafka-bootstrap-kafka-qa-e1.qa.prismacloud.io').split()
    env['cassandra'] = os.getenv("CASSANDRA", 'cassandra').split()
    env['elasticsearch'] = os.getenv("ELASTICSEARCH", "localhost:9200")

    # public alerts service endpoint
    env['alerts_server'] = os.getenv("ALERTS_SERVER", 'api14.qa.prismacloud.io')

    # public api login
    env['hostname'] = os.getenv("HOSTNAME", None)
    env['user'] = os.getenv("USER", "qa@redlock.io")
    env['password'] = os.getenv("PWD", "DevTemplate.3905!")
    env['customer'] = os.getenv("CUSTOMER", "PANW-dev")

    env['customerId'] = os.getenv("CUSTOMERID", 3)

    # Internal alerts service endpoint
    env['api_rest'] = os.getenv("API_REST", 'api14.qa.prismacloud.io')
    env['api_aggregator'] = os.getenv("API_AGGREGATOR", 'api14.qa.prismacloud.io')
    cur_longrunning_key = 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImFsZXJ0cy1zZXJ2aWNlLWtleSJ9' \
                          '.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTU'\
                          'xNjIzOTAyMn0.jgCCxQrm2P8h_BqUU-MnAq3eod4LUAQLwJCJMYWwWUZCpfOD_MmDoKpiwfRKmFZXgYp'\
                          'rlXiac9hXUpMulK9i9rN_QEBmAEyQwqGrsv54LfbAu5c8Xwmpl2TDVvL2_Zz49gK97CSpiltd8TduZz3'\
                          '3AqzCLiF6qolon1q2ANOuWcYkjNx-Pna88-y0-iZk7VHvr9NV8OwMSpxEmjw5XRwVzkoo0EBSNYE2yLM'\
                          '71yPUkPlE2txSAkRpLScRm2HHJyaVQKGz5m1NJiJ-DnFDkQJxuPaEbQU1L9_kCmzX_qWDN-A-mEMGdq5'\
                          'thCAN3rqZ423aSuRzfHiV0Fv0xJH8k5X3Ew '
    env['auth_key'] = os.getenv("AUTH_KEY", cur_longrunning_key)

    # db connect para
    env['db_host'] = os.getenv("DB_HOST", None)
    env['db_port'] = os.getenv("DB_PORT", 5432)
    env['db_user'] = os.getenv("DB_USER", 'runtime')
    env['db_pwd'] = os.getenv("DB_PWD", None)
    env['ssh_host'] = os.getenv("SSH_HOST", None)
    env['ssh_user'] = os.getenv("SSH_USER", None)
    env['ssh_private_key'] = os.getenv("SSH_PRIKEY", None)

    # Kafka Rest Proxy
    env['krp_auth_key'] = os.getenv("AUTH_KEY", cur_longrunning_key)

    return env


@pytest.fixture(scope='class')
def public_api_fixture(request, testbed):
    session = PublicApi(hostname=testbed['hostname'],
                        user=testbed['user'],
                        password=testbed['password'],
                        customer=testbed['customer'])
    print(session.rest_login())
    request.cls.restlib = session
    return session
