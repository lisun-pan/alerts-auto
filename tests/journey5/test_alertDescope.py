import os
import json
import time
import pytest
import requests

from libs.utils.http_client import HttpClient
from libs.utils.logger import get_logger
from libs.utils.pytest_utils import get_test_data_file_content
from libs.utils.elasticsearch_util import ElasticSearchDB
from libs.utils.kafka_client import KafkaClient
from libs.utils.cassandra_util import DBOperator

payloads = get_test_data_file_content('data/alert_tests.json')
logger = get_logger(os.path.basename(__file__).replace(".py", ""))
global search_index
search_index = 0

def get_test_names(test_data):
    """
     Method to return the list of tests reading the test data file content
    """
    test_names = []
    print("Entering get_test_names...")
    for test_name in list(payloads.keys()):
        print("testname:", test_name)
        logger.info("testname:", test_name)
        print("input:", test_data)
        if test_name == test_data:
            for tests in list(payloads[test_name].keys()):
                print("tests:", tests)
                logger.info("tests:", tests)
                test_names.append(pytest.param(tests, id=tests))
                print("test names:", test_names)
                logger.info("test names:", test_names)

    return test_names


# Clean up the tests - Change the alert status to OPEN again
def cleanUp(alertId,testbed):

    logger.info("Cleaning up..update the alert back to Open")
    alert_payload = {
          "status": "OPEN",
          "snoozeExpiryTs": 4352345353000,
          "originalSnoozeDuration" : "6 MONTH",
          "statusChangeReason" : "Grahith didn't like it"
    }
    # alertId = alert_payload[0]["searchRequest"]["filters"]["alertIds"]
    # alertId = alertId[0]
    alerts_search = "%s:9081/alerts/v1/alertRecord/%s/status" % (testbed['alerts_server'], alertId)
    client = HttpClient()
    status, response = client.rest_send_request(uri=alerts_search, method='PUT',
                                                data=json.dumps(alert_payload))
    logger.info(response)
    assert status == requests.codes.ok,"Expected response is 200 OK from the REST service"
    print("Response Status:", status)
    logger.info("Response Status:", status)


@pytest.mark.INTEGRATION
@pytest.mark.parametrize('test_name', get_test_names('alert_descope_main'))
def test_bulk_writer(test_name, testbed):

    print("Entering the bulk writer...")
    logger.info("Entering the bulk worker...")

    # Bulk post the data to the REST endpoint
    payload = payloads["alert_descope_main"]["alert_descope"]
    alerts_endpoint = "%s:9081/alerts/v1/policyVerdict" % testbed['alerts_server']
    # for each_payload in payload:
    print("Running Tests....POST alerts to REST service")
    logger.info("Running Tests....POST alerts to REST service")
    # test_payload = [each_payload]
    test_payload = payload
    print("Request Payload for all the De-scope tests:", test_payload)
    logger.info(test_payload)
    client = HttpClient()
    status, response = client.rest_send_request(uri=alerts_endpoint, method='post',
                                                data=json.dumps(test_payload))
    logger.info(response)
    print("Response:", status)
    logger.info("Response:", status)
    assert status == 202,"Expected response is 202 from the :9081/alerts/v1/policyVerdict"

    for each_payload in payload:

        # Connect to the Cassandra and verify the records in the DB
        print("__________________________________________________")
        print("Verifying the records in Cassandra DB ...")
        print("--------------------------------------------------")
        logger.info("__________________________________________________")
        logger.info("Verifying the records in Cassandra DB ...")
        logger.info("--------------------------------------------------")
        cassandra_obj = DBOperator()
        session = cassandra_obj.connect_to_db()
        customer_id = each_payload['customerId']
        logger.info("customerId:", customer_id)
        rows = session.execute("select * from local_redlock.alert where customer_id='%s' ALLOW FILTERING;" %customer_id)
        print("Cassandra Records:")
        for row in rows:
            print(row)
            assert len(row) != 0,"Expected atleast one record from Cassandra"

        rows = session.execute("select status from local_redlock.alert where customer_id='%s' ALLOW FILTERING;" %customer_id)
        for row in rows:
            print("Status of the Alert:", row.status)
            #assert row.status == "OPEN"


@pytest.mark.INTEGRATION
@pytest.mark.parametrize('test_name', get_test_names('alert_descope_tests'))
def test_descope_alert(test_name, testbed):
    print("__________________________________________________")
    print("De-Scoping the alert...")
    print("--------------------------------------------------")
    logger.info("Descoping the alert...")
    global search_index

    # De-scope the alert - HTTP PUT to the Alert REST Service
    alerts_descope = "%s:9081/alerts/v1/descope" % testbed['alerts_server']
    client = HttpClient()
    payload_descope = payloads["alert_descope_tests"][test_name]

    for each_payload_descope in payload_descope:

        print("De-scope payload:", each_payload_descope)
        logger.info("De-scope payload:", each_payload_descope)
        status, response = client.rest_send_request(uri=alerts_descope, method='PUT',
                                                    data=json.dumps(each_payload_descope))
        logger.info(response)
        assert status == 202, "Expected response is 202 for the PUT request to /v1/descope endpoint"

        # Verification of the alert status in Cassandra
        time.sleep(10)
        # Connect to the Cassandra and verify the records in the DB
        print("__________________________________________________")
        print("Verifying the records in Cassandra DB ...")
        print("--------------------------------------------------")
        logger.info("__________________________________________________")
        logger.info("Verifying the records in Cassandra DB ...")
        logger.info("--------------------------------------------------")
        cassandra_obj = DBOperator()
        session = cassandra_obj.connect_to_db()
        customer_id = each_payload_descope['customerId']

        rows = session.execute(
            "select * from local_redlock.alert where customer_id='%s' ALLOW FILTERING;" % customer_id)
        print("Cassandra Records:")
        logger.info("Cassandra Records:")
        for row in rows:
            print(row)
            assert len(row) != 0
        rows = session.execute(
            "select status from local_redlock.alert where customer_id='%s' ALLOW FILTERING;" % customer_id)
        for row in rows:
            print("Alert Status:", row.status)
            #assert row.status == "RESOLVED"


        # The alert Id is procured from the search service
        alerts_search = "%s:9084/alerts/v1/search" % testbed['alerts_server']
        client = HttpClient()
        each_payload_search = payloads["alert_search_main"]["Search_to_Descope"][search_index]
        search_index +=1
        #for each_payload_search in payload_search:

        print("__________________________________________________")
        print("Searching for the alert:")
        print("--------------------------------------------------")
        print("search payload:",each_payload_search)
        logger.info("search payload:",each_payload_search)
        status, response = client.rest_send_request(uri=alerts_search, method='POST',
                                                    data=json.dumps(each_payload_search))
        logger.info(response)
        assert status == requests.codes.ok, "Expected Response for the alert search is 200 OK from the search service"

        print("Response Status:", status)
        logger.info("Response Status:", status)
        responseObj = json.loads(response.decode("utf-8"))
        print("response payload:", responseObj)
        logger.info("response payload:", responseObj)
        alertId = responseObj[0]['alertId']
        print("alertId:", responseObj[0]['alertId'])
        logger.info("alertId:", responseObj[0]['alertId'])
        logger.info("response payload:", responseObj)
        assert len(responseObj) != 0
        for each_resp_obj in responseObj:
            print(each_resp_obj)
            logger.info(each_resp_obj)
            assert len(each_resp_obj) != 0

        time.sleep(10)
        # Search for the above alert
        print("__________________________________________________")
        print("Search for the above alertId status")
        print("--------------------------------------------------")
        logger.info("Search for the alert")
        alerts_search = "%s:9081/alerts/v1/alertRecord/%s" % (testbed['alerts_server'],alertId)
        client = HttpClient()
        status, response = client.rest_send_request(uri=alerts_search, method='GET')
        logger.info(response)
        assert status == requests.codes.ok
        print("Response Status:", status)
        logger.info("Response Status:", status)
        responseObj = json.loads(response.decode("utf-8"))
        print("response payload:", responseObj)
        logger.info("response payload:", responseObj)
        assert responseObj['alertStatus'] == 5, "Expected Alert Status is 5 (RESOLVED)"

        # Clean up the alerts back to OPEN status
        cleanUp(alertId,testbed)

        # Search for the above alert
        print("Search for the alert status again  - should be OPEN - 1:")
        logger.info("Search for the alert status again - should be OPEN - 1:")
        alerts_search = "%s:9081/alerts/v1/alertRecord/%s" % (testbed['alerts_server'], alertId)
        client = HttpClient()
        status, response = client.rest_send_request(uri=alerts_search, method='GET')
        logger.info(response)
        assert status == requests.codes.ok
        print("Response Status:", status)
        logger.info("Response Status:", status)
        responseObj = json.loads(response.decode("utf-8"))
        print("response payload:", responseObj)
        logger.info("response payload:", responseObj)
        assert responseObj['alertStatus'] == 1, "Expected Alert Status is 1 (OPEN)"



