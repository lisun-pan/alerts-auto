import json
import string
import random
from time import sleep

import pytest
import requests

from libs.utils.http_client import HttpClient
from libs.utils.public_api import PublicApi


def get_alert_status(url, alert_id, headers):
    client = HttpClient()
    print("Do /v1/alertRecord/ get")
    alert_endpoint = "%s/alerts/v1/alertRecord/%s" % (url, alert_id)
    status, response = client.rest_send_request(uri=alert_endpoint, method='get', headers=headers)
    return status, response


@pytest.fixture(scope="module")
def add_alerts(request, testbed):
    return create_alerts(request.param, testbed)


@pytest.fixture(scope="module")
def add_policies(request, testbed):
    return policy_create(request.param, testbed)


@pytest.fixture(scope="module")
def delete_policies(request, testbed):
    return policies_delete(request.param, testbed)


@pytest.fixture(scope="module")
def delete_accounts(request, testbed):
    return account_delete(request.param, testbed)


def create_alerts(payloads, testbed):
    """
    :param testbed:
    :param payloads: for creating the new alerts
    :return:
    """
    # post alerts
    customID = generateCustomID()
    alerts_payloads = payloads["alerts_bulk_writer"]
    alerts_create_endpoint = "%s/alerts/v1/policyVerdict" % testbed['api_rest']
    client = HttpClient()
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    replaceCustomID(alerts_payloads, customID)
    print("Do /alerts/v1/policyVerdict post")
    status, response = client.rest_send_request(uri=alerts_create_endpoint, method='post', headers=headers,
                                                data=json.dumps(alerts_payloads))
    assert status == requests.codes.accepted, alerts_payloads

    # get the new alerts' ids
    print("Do /alerts/v1/search post")
    alerts_search_endpoint = "%s/alerts/v1/search" % testbed['api_aggregator']
    search_payloads = payloads["alerts_search"]
    search_payloads['customerId'] = customID
    sleep(20)

    status, response = client.rest_send_request(uri=alerts_search_endpoint, method='post', headers=headers,
                                                data=json.dumps(search_payloads))
    assert status == requests.codes.ok, "error when get alert by /alerts/v1/search"
    responseObjs = json.loads(response.decode("utf-8"))
    alerts_ids = []

    for obj in responseObjs['searchResults']:
        alerts_ids.append(obj["alertId"])
    # check the alerts status
    client = HttpClient()
    for alert_id in alerts_ids:
        print("Do /alerts/v1/alertRecord get")
        alert_endpoint = "%s/alerts/v1/alertRecord/%s" % (testbed['api_rest'], alert_id)
        status, response = client.rest_send_request(uri=alert_endpoint, method='get', headers=headers)
        assert status == requests.codes.ok, "error when get alert by alerts/v1/alertRecord"
        responseObj = json.loads(response.decode("utf-8"))
        assert responseObj["alertStatus"] == 1

    return alerts_ids, customID


def generateCustomID(stringLength=6):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


def replaceCustomID(payloads, custom_id):
    for payload in payloads:
        payload['customerId'] = custom_id


def policy_create(payloads, testbed):
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")

    policy_create_endpoint = "https://%s/policy" % testbed['alerts_server']
    print("url:", policy_create_endpoint)
    policy_create_payloads = payloads["policy_create_txt"]
    print("payload:", policy_create_payloads)

    client.rest_login()
    status, response = client.rest_send_request(uri=policy_create_endpoint, method='POST', headers=headers,
                                                data=json.dumps(policy_create_payloads))
    # assert status == requests.codes.created, policy_create_payloads
    # assert status == 200
    # return 1


def policies_delete(payloads, testbed):
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")
    # Get All the policies
    policy_getall_endpoint = "https://%s/policy?policy.type=data" % testbed['alerts_server']
    policy_create_payloads = payloads["policy_create_txt"]
    client.rest_login()
    status, response = client.rest_send_request(uri=policy_getall_endpoint, method='GET', headers=headers)
    # assert status == requests.codes.created, policy_create_payloads
    # assert status == 200, response

    responseObj = json.loads(response)
    for i in range(len(responseObj)):
        print("PolicyIds:", responseObj[i]['policyId'])
        policyId = responseObj[i]['policyId']
        policy_delete_endpoint = "https://%s/policy/%s" % (testbed['alerts_server'], policyId)
        status, response = client.rest_send_request(uri=policy_delete_endpoint, method='DELETE', headers=headers)
        # assert status == 204


def account_delete(test_name, testbed):
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")
    # client = PublicApi(hostname="api14.qa.prismacloud.io", user="", password="")

    account_create_endpoint = "https://%s/cloud/aws/349006084872" % testbed['alerts_server']
    client.rest_login()
    status, response = client.rest_send_request(uri=account_create_endpoint, method='DELETE', headers=headers)

    # assert status == requests.codes.created, policy_create_payloads
    # assert status == 200, response


if __name__ == '__main__':
    print(generateCustomID())
