import json
import os
import pytest

from libs.utils.public_api import PublicApi
from libs.utils.logger import get_logger
from libs.utils.pytest_utils import get_test_data_file_content
from libs.utils.kafka_client import KafkaClient


payloads = get_test_data_file_content('data/policy_tests.json')
logger = get_logger(os.path.basename(__file__).replace(".py", ""))


def get_test_names(test_data):
    """
     Method to return the list of tests reading the test data file content
    """
    test_names = []
    print("Entering get_test_names...")
    for test_name in list(payloads.keys()):
        print("testname:", test_name)
        print("input:", test_data)
        if test_name == test_data:
            for tests in list(payloads[test_name].keys()):
                print("tests:", tests)
                test_names.append(pytest.param(tests, id=tests))
                print("test names:", test_names)

    return test_names


@pytest.mark.API
@pytest.mark.POLICY
@pytest.mark.parametrize('delete_policies', [payloads["policy_main"]], indirect=True)
@pytest.mark.parametrize('test_name', get_test_names('policy_main'))
def test_policy_create(test_name, testbed, delete_policies):

    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com", password="Changeme!23")
    #client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
    #                  password=testbed["password"])

    policy_create_endpoint = "https://%s/policy" % testbed['alerts_server']
    print("url:",policy_create_endpoint)
    policy_create_payloads = payloads["policy_main"][test_name]
    print("payload:",policy_create_payloads)

    c1 = client.rest_login()
    status, response = client.rest_send_request(uri=policy_create_endpoint, method='POST', headers=headers,
                                                data=json.dumps(policy_create_payloads))

    assert status == 200

    '''
    # If we have to run the test on app14 directly uncomment the code
    # Consuming from Policy_change_topic in right side kafka
    print("__________________________________________________")
    print("Verifying Kafka Reads - policy_change_topic")
    kafka_consumer = KafkaClient(kafka_servers=testbed['kafka_servers'])
    reads = kafka_consumer.read_topic(testbed['policy_change_topic'])
    print("kafka policy_change topic reads:", reads)
    assert len(reads) != 0
    '''
    krp = KafkaClient()
    krp_diff = krp.read_kafka_rest_proxy('policy_change_topic',testbed)
    assert len(krp_diff) > 0, "previous and current jsons are the same"


@pytest.mark.API
@pytest.mark.POLICY
@pytest.mark.parametrize('add_policies', [payloads["policy_main"]], indirect=True)
@pytest.mark.parametrize('test_name', get_test_names('policy_get'))
def test_policy_getall(test_name, testbed, add_policies):

    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}

    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")
    #client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
    #                   password=testbed["password"])
    policy_create_endpoint = "https://%s/policy?policy.type=data" % testbed['alerts_server']
    policy_create_payloads = payloads["policy_get"][test_name]
    client.rest_login()
    status, response = client.rest_send_request(uri=policy_create_endpoint, method='GET', headers=headers)
    assert status == 200, response
    responseObj = json.loads(response)
    for i in range(len(responseObj)):
        print("PolicyIds:", responseObj[i]['policyId'])


@pytest.mark.API
@pytest.mark.POLICY
#@pytest.mark.parametrize('delete_policies', [payloads["policy_main"]], indirect=True)
@pytest.mark.parametrize('add_policies', [payloads["policy_main"]], indirect=True)
@pytest.mark.parametrize('test_name', get_test_names('policy_getbyId'))
def test_policy_getbyId(test_name, testbed, add_policies):

    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")
    #client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
    #                   password=testbed["password"])
    # Get All the policies
    policy_getall_endpoint = "https://%s/policy?policy.type=data" % testbed['alerts_server']
    client.rest_login()
    status, response = client.rest_send_request(uri=policy_getall_endpoint, method='GET', headers=headers)
    assert status == 200, response
    responseObj = json.loads(response)

    # Pick one policy from the above list
    policyId = responseObj[0]['policyId']
    policy_getbyId_endpoint = "https://%s/policy/%s" % (testbed['alerts_server'], policyId)
    policy_getbyId_payloads = payloads["policy_getbyId"][test_name]
    status, response = client.rest_send_request(uri=policy_getbyId_endpoint, method='GET', headers=headers)
    assert status == 200, policy_getbyId_payloads


@pytest.mark.API
@pytest.mark.POLICY
@pytest.mark.parametrize('test_name', get_test_names('policy_deletebyId'))
def test_policy_deletebyId(test_name, testbed):

    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")
    #client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
    #                   password=testbed["password"])
    # Get All the policies
    policy_getall_endpoint = "https://%s/policy?policy.type=data" % testbed['alerts_server']
    client.rest_login()
    status, response = client.rest_send_request(uri=policy_getall_endpoint, method='GET', headers=headers)
    assert status == 200, response
    responseObj = json.loads(response)

    # Pick one policy from the above list
    policyId = responseObj[0]['policyId']
    policy_delete_endpoint = "https://%s/policy/%s" % (testbed['alerts_server'], policyId)
    policy_delete_payloads = payloads["policy_deletebyId"][test_name]
    status, response = client.rest_send_request(uri=policy_delete_endpoint, method='DELETE', headers=headers)
    assert status == 204, policy_delete_payloads

    '''
    # If we have to run the test on app14 directly uncomment the code
    # Consuming from Policy_change_topic in right side kafka
    print("__________________________________________________")
    print("Verifying Kafka Reads - policy_change_topic")
    kafka_consumer = KafkaClient(kafka_servers=testbed['kafka_servers'])
    reads = kafka_consumer.read_topic(testbed['policy_change_topic'])
    print("kafka policy_change topic reads:", reads)
    assert len(reads) != 0
    '''

@pytest.mark.API
@pytest.mark.POLICY
@pytest.mark.parametrize('test_name', get_test_names('policy_suggest'))
def test_policy_suggest(test_name, testbed):

    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")
    #client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
    #                   password=testbed["password"])
    policy_create_endpoint = "https://%s/filter/policy/suggest" % testbed['alerts_server']
    client.rest_login()
    status, response = client.rest_send_request(uri=policy_create_endpoint, method='GET', headers=headers)
    assert status == 200, response

@pytest.mark.API
@pytest.mark.POLICY
@pytest.mark.parametrize('test_name', get_test_names('policy_update'))
def test_policy_update(test_name, testbed):

    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                      password="Changeme!23")
    #client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
    #                   password=testbed["password"])
    policy_update_payloads = payloads["policy_update"][test_name]
    policyId = policy_update_payloads["policyId"]
    policy_update_endpoint = "https://%s/policy/%s" % (testbed['alerts_server'], policyId)
    status, response = client.rest_send_request(uri=policy_update_endpoint, method='PUT', headers=headers,
                                                data=json.dumps(policy_update_payloads))
    assert status == 200, response

    '''
    # If we have to run the test on app14 directly uncomment the code
    # Consuming from Policy_change_topic in right side kafka
    print("__________________________________________________")
    print("Verifying Kafka Reads - policy_change_topic")
    kafka_consumer = KafkaClient(kafka_servers=testbed['kafka_servers'])
    reads = kafka_consumer.read_topic(testbed['policy_change_topic'])
    print("kafka policy_change topic reads:", reads)
    assert len(reads) != 0
    '''
    krp = KafkaClient()
    krp_diff = krp.read_kafka_rest_proxy('policy_change_topic',testbed)
    assert len(krp_diff) >0, "previous and current jsons are the same"
