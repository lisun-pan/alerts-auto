import json
import os
from time import sleep

import pytest
import requests

from libs.utils.http_client import HttpClient
from libs.utils.public_api import PublicApi
from libs.utils.logger import get_logger
from libs.utils.pytest_utils import get_test_data_file_content
from tests.conftest import testbed
from libs.utils.kafka_client import KafkaClient

payloads = get_test_data_file_content('data/accountgroups_tests.json')
logger = get_logger(os.path.basename(__file__).replace(".py", ""))

def get_test_names(test_data):
    """
     Method to return the list of tests reading the test data file content
    """
    test_names = []
    print("Entering get_test_names...")
    for test_name in list(payloads.keys()):
        print("testname:", test_name)
        print("input:", test_data)
        if test_name == test_data:
            for tests in list(payloads[test_name].keys()):
                print("tests:", tests)
                test_names.append(pytest.param(tests, id=tests))
                print("test names:", test_names)

    return test_names


@pytest.mark.API
@pytest.mark.parametrize('test_name', get_test_names('accountgroup_get'))
def test_accountgroup_getall(test_name, testbed):

    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")
    account_getall_endpoint = "https://%s/cloud/group" % testbed['alerts_server']

    c1 = client.rest_login()
    status, response = client.rest_send_request(uri=account_getall_endpoint, method='GET', headers=headers)
    assert status == 200, response
    responseObj = json.loads(response)

    for i in range(len(responseObj)):
       print("accountIds:", responseObj[i]['name'])


@pytest.mark.API
@pytest.mark.parametrize('test_name', get_test_names('accountgroup_getbyId'))
def test_accountgroup_getbyId(test_name, testbed):

    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")

    # Get all accountgroupIds
    account_getall_endpoint = "https://%s/cloud/group" % testbed['alerts_server']
    client.rest_login()
    status, response = client.rest_send_request(uri=account_getall_endpoint, method='GET', headers=headers)
    assert status == 200, response

    responseObj = json.loads(response)

    for i in range(len(responseObj)):

        # Pick one policy from the above list
        accountGroupId = responseObj[i]['id']

        accountGroup_getbyId_endpoint = "https://%s/cloud/group/%s" % (testbed['alerts_server'], accountGroupId)
        accountGroup_getbyId_payloads = payloads["accountgroup_getbyId"][test_name]
        status, response = client.rest_send_request(uri=accountGroup_getbyId_endpoint, method='GET', headers=headers)
        assert status == 200


@pytest.mark.API
@pytest.mark.parametrize('test_name', get_test_names('accountgroup_update'))
def test_accountgroup_update(test_name, testbed):

    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")

    # Account Group update
    accountGroup_update_payloads = payloads["accountgroup_update"][test_name]
    accountGroupId = accountGroup_update_payloads["id"]

    print("AccountGroupId:",accountGroupId)
    accountGroup_update_endpoint = "https://%s/cloud/group/%s" % (testbed['alerts_server'], accountGroupId)

    status, response = client.rest_send_request(uri=accountGroup_update_endpoint, method='PUT', headers=headers,
                                                data=json.dumps(accountGroup_update_payloads))
    assert status == 200, accountGroup_update_payloads


@pytest.mark.API
@pytest.mark.parametrize('test_name', get_test_names('accountgroup_delete'))
def test_accountgroup_deletebyId(test_name, testbed):

    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")

    # Create Account Group
    accountGroup_create_endpoint = "https://%s/cloud/group" % testbed['alerts_server']
    accountGroup_create_payloads = payloads["accountgroup_delete"][test_name]
    client.rest_login()
    status, response = client.rest_send_request(uri=accountGroup_create_endpoint, method='POST', headers=headers,
                                                data = json.dumps(accountGroup_create_payloads))

    # Get all accountgroupIds
    # policy_create_payloads = payloads["policy_getbyId"][test_name]
    client.rest_login()
    status, response = client.rest_send_request(uri=accountGroup_create_endpoint, method='GET', headers=headers)
    assert status == 200, response

    responseObj = json.loads(response)
    for i in range(len(responseObj)):
        # Pick one policy from the above list
        accountGroupId = responseObj[i]['id']
        accountGroupName = responseObj[i]['name']
        print("accountGroupId:",accountGroupId, "accountGroupName:", accountGroupName)

        if responseObj[i]['name'] == 'test-account-group-delete':
            accountGroupId_toDelete = responseObj[i]['id']
            accountGroup_delete_endpoint = "https://%s/cloud/group/%s" % (testbed['alerts_server'],accountGroupId_toDelete)
            accountGroup_delete_payloads = payloads["accountgroup_delete"][test_name]
            status, response = client.rest_send_request(uri=accountGroup_delete_endpoint, method='DELETE',
                                                        headers=headers)

            assert status == 200, response

