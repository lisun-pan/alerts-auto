import json
import os
from time import sleep

import pytest
import requests

from libs.utils.http_client import HttpClient
from libs.utils.public_api import PublicApi
from libs.utils.logger import get_logger
from libs.utils.pytest_utils import get_test_data_file_content
from tests.conftest import testbed
from libs.utils.kafka_client import KafkaClient

payloads = get_test_data_file_content('data/account_tests.json')
logger = get_logger(os.path.basename(__file__).replace(".py", ""))


def get_test_names(test_data):
    """
     Method to return the list of tests reading the test data file content
    """
    test_names = []
    print("Entering get_test_names...")
    for test_name in list(payloads.keys()):
        print("testname:", test_name)
        print("input:", test_data)
        if test_name == test_data:
            for tests in list(payloads[test_name].keys()):
                print("tests:", tests)
                test_names.append(pytest.param(tests, id=tests))
                print("test names:", test_names)

    return test_names


@pytest.mark.API
@pytest.mark.parametrize('delete_accounts', [payloads["account_main"]], indirect=True)
@pytest.mark.parametrize('test_name', get_test_names('account_main'))
def test_account_create(test_name, testbed, delete_accounts):
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")
    account_create_endpoint = "https://%s/cloud/aws" % testbed['alerts_server']
    print("url:", account_create_endpoint)
    account_create_payloads = payloads["account_main"][test_name]

    client.rest_login()
    status, response = client.rest_send_request(uri=account_create_endpoint, method='POST', headers=headers,
                                                data=json.dumps(account_create_payloads))
    assert status == 200, response


@pytest.mark.API
@pytest.mark.parametrize('test_name', get_test_names('account_get'))
def test_account_getall(test_name, testbed):
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")
    account_getall_endpoint = "https://%s/cloud" % testbed['alerts_server']
    account_getall_payloads = payloads["account_get"][test_name]
    client.rest_login()
    status, response = client.rest_send_request(uri=account_getall_endpoint, method='GET', headers=headers)
    assert status == 200, response
    responseObj = json.loads(response)

    for i in range(len(responseObj)):
        print("accountIds:", responseObj[i]['name'])


@pytest.mark.API
@pytest.mark.parametrize('test_name', get_test_names('account_getbyId'))
def test_account_getbyId(test_name, testbed):
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")

    # Get all accountIds
    account_getall_endpoint = "https://%s/cloud" % testbed['alerts_server']
    client.rest_login()
    status, response = client.rest_send_request(uri=account_getall_endpoint, method='GET', headers=headers)
    # assert status == requests.codes.created, policy_create_payloads
    assert status == 200, response
    responseObj = json.loads(response)

    for i in range(len(responseObj)):
        # Pick one policy from the above list
        accountId = responseObj[i]['accountId']
        accountType = responseObj[i]['cloudType']
        account_getbyId_endpoint = "https://%s/cloud/%s/%s" % (testbed['alerts_server'], accountType, accountId)
        account_getbyId_payloads = payloads["account_getbyId"][test_name]
        status, response = client.rest_send_request(uri=account_getbyId_endpoint, method='GET', headers=headers)
        assert status == 200, account_getbyId_payloads


@pytest.mark.API
@pytest.mark.parametrize('test_name', get_test_names('account_update'))
def test_account_update(test_name, testbed):
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")

    # Account update
    account_update_payloads = payloads["account_update"][test_name]
    accountType = '%s' % test_name
    if accountType == 'aws':
        accountId = account_update_payloads["accountId"]
    elif accountType == 'azure':
        accountId = account_update_payloads["cloudAccount"]["accountId"]
    elif accountType == 'gcp':
        accountId = account_update_payloads["cloudAccount"]["accountId"]

    account_update_endpoint = "https://%s/cloud/%s/%s" % (testbed['alerts_server'], accountType, accountId)

    status, response = client.rest_send_request(uri=account_update_endpoint, method='PUT', headers=headers,
                                                data=json.dumps(account_update_payloads))
    assert status == 200, account_update_payloads


@pytest.mark.API
@pytest.mark.parametrize('test_name', get_test_names('account_status'))
def test_account_status(test_name, testbed):
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    client = PublicApi(hostname="api14.qa.prismacloud.io", user="hduriseti@paloaltonetworks.com",
                       password="Changeme!23")

    # Account update
    account_update_payloads = payloads["account_status"][test_name]
    accountType = '%s' % test_name
    if accountType == 'aws':
        accountId = account_update_payloads["accountId"]
    elif accountType == 'azure':
        accountId = account_update_payloads["cloudAccount"]["accountId"]
    elif accountType == 'gcp':
        accountId = account_update_payloads["cloudAccount"]["accountId"]

    account_update_endpoint = "https://%s/cloud/status/%s" % (testbed['alerts_server'], accountType)
    status, response = client.rest_send_request(uri=account_update_endpoint, method='POST', headers=headers,
                                                data=json.dumps(account_update_payloads))
    assert status == 200, account_update_payloads
