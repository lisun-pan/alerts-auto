import json
import os
from time import sleep

import pytest
import requests

from libs.utils.http_client import HttpClient
from libs.utils.logger import get_logger
from libs.utils.pytest_utils import get_test_data_file_content
from tests.integration.conftest import get_alert_status, replaceCustomID, generateCustomID

payloads = get_test_data_file_content('data/api_tests.json')
logger = get_logger(os.path.basename(__file__).replace(".py", ""))

###
# 1 - OPEN
# 2- DISMISSED
# 3 - SNOOZED
# 4 - PENDING RESOLUTION
# 5 - RESOLVED
##

status_map = {
    "open_to_snoozed": 3,
    "snoozed_to_open": 1,
    "open_to_dismissed": 2,
    "dismissed_to_open": 1,
    "open_to_pending": 4,
    "snoozed_to_dismissed": 2,
    "dismissed_to_snoozed": 3,
    "pending_to_snoozed": 3,
    "pending_to_dismissed": 2,
    "pending_to_open": 1

}

pass_status = ["open_to_snoozed", "snoozed_to_open", "open_to_dismissed",
               "dismissed_to_open", "open_to_pending"]


def get_test_names(test_kind, test_data):
    """
     Method to return the list of tests reading the test data file content
    """
    test_names = []
    for test_name in list(payloads[test_kind].keys()):
        if test_name == test_data:
            for tests in list(payloads[test_kind][test_name].keys()):
                test_names.append(pytest.param(tests, id=tests))

    return test_names


def replaceBulkUpdate(payload, custom_id):
    payload['searchRequest']['customerId'] = custom_id


@pytest.mark.API
@pytest.mark.parametrize('add_alerts', [payloads["alerts_bulk_update"]], indirect=True)
@pytest.mark.parametrize('test_name', get_test_names('alerts_bulk_update', 'alert_statues_bulk_update'))
def test_bulk_update(add_alerts, test_name, testbed):
    """
        Test Steps:
        1. post alerts to /alerts;
        2. get the alert ids which are new generated
        2. check the alerts status which should be open;
        3. do bulk worker which dismiss the alerts;
        4. check the alerts status which should be dismissed;
    """
    alerts_ids, customID = add_alerts
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    # if test case is a negative test case
    cur_status = -1
    if test_name not in pass_status:
        status, response = get_alert_status(testbed['api_rest'], alerts_ids[0], headers=headers)
        assert status == requests.codes.ok, "error when get alert by /alerts/v1/alertRecord/"
        responseObj = json.loads(response.decode("utf-8"))
        cur_status = responseObj["alertStatus"]
    # do bulk worker which dismiss the alerts
    client = HttpClient()
    alerts_bulk_update_endpoint = "%s/alerts/v1/bulk/statusJob" % testbed['api_rest']
    bulk_update_payloads = payloads["alerts_bulk_update"]["alert_statues_bulk_update"][test_name]
    replaceBulkUpdate(bulk_update_payloads, customID)
    print("Do /v1/bulk/statusJob post")
    status, response = client.rest_send_request(uri=alerts_bulk_update_endpoint, method='put', headers=headers,
                                                data=json.dumps(bulk_update_payloads))
    assert status == requests.codes.created, bulk_update_payloads
    sleep(30)
    # check the alerts status which should be dismissed
    for alert_id in alerts_ids:
        status, response = get_alert_status(testbed['api_rest'], alert_id, headers=headers)
        assert status == requests.codes.ok
        responseObj = json.loads(response.decode("utf-8"))
        if test_name in pass_status:
            assert responseObj["alertStatus"] == status_map[test_name], bulk_update_payloads
        else:
            assert responseObj["alertStatus"] == cur_status, bulk_update_payloads


@pytest.mark.API
@pytest.mark.parametrize('add_alerts', [payloads["alert_single_update"]], indirect=True)
@pytest.mark.parametrize('test_name', get_test_names('alerts_bulk_update', 'alert_statues_bulk_update'))
def test_single_update(add_alerts, test_name, testbed):
    """
        Test Steps:
        1. Add an alert;
        2. get the alert id which are new generated
        2. check the alert's status which should be open;
        3. do single update which snoozed the alerts;
        4. check the alert's status which should be snoozed;
    """
    alerts_ids, customID = add_alerts
    assert len(alerts_ids) == 1
    alert_id = alerts_ids[0]
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    # if test case is a negative test case
    cur_status = -1
    if test_name not in pass_status:
        status, response = get_alert_status(testbed['api_rest'], alert_id, headers=headers)
        assert status == requests.codes.ok
        responseObj = json.loads(response.decode("utf-8"))
        cur_status = responseObj["alertStatus"]

    # do bulk worker which dismiss the alerts
    client = HttpClient()
    alerts_single_update_endpoint = "%s/alerts/v1/alertRecord/%s/status" % (
        testbed['api_rest'], alert_id)
    bulk_single_payloads = payloads["alert_single_update"]["alert_statues_single_update"][test_name]
    status, response = client.rest_send_request(uri=alerts_single_update_endpoint, method='put', headers=headers,
                                                data=json.dumps(bulk_single_payloads))

    if test_name in pass_status:
        assert status == requests.codes.ok, bulk_single_payloads
    else:
        assert status == requests.codes.bad_request, bulk_single_payloads

    sleep(30)
    # check the alerts status which should be dismissed
    status, response = get_alert_status(testbed['api_rest'], alert_id, headers=headers)
    assert status == requests.codes.ok
    # single update will fail for the negative test cases throw 400
    responseObj = json.loads(response.decode("utf-8"))
    if test_name in pass_status:
        assert responseObj["alertStatus"] == status_map[test_name], bulk_single_payloads
    else:
        assert responseObj["alertStatus"] == cur_status, bulk_single_payloads


@pytest.mark.API
def test_dlp_meta_alerts(testbed):
    bulk_writer_payload = payloads["alert_dlp_meta"]["alerts_bulk_writer"]
    customID = generateCustomID()
    print(f'CustomID is {customID}')
    replaceCustomID(bulk_writer_payload, customID)
    alerts_create_endpoint = "%s/alerts/v1/policyVerdict" % testbed['api_rest']
    client = HttpClient()
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    status, response = client.rest_send_request(uri=alerts_create_endpoint, method='post', headers=headers,
                                                data=json.dumps(bulk_writer_payload))
    assert status == requests.codes.accepted, bulk_writer_payload

    alerts_search_endpoint = "%s/alerts/v1/search" % testbed['api_aggregator']
    search_payloads = payloads["alert_dlp_meta"]["alerts_search"]
    search_payloads['customerId'] = customID
    sleep(20)
    status, response = client.rest_send_request(uri=alerts_search_endpoint, method='post', headers=headers,
                                                data=json.dumps(search_payloads))
    assert status == requests.codes.ok, "error when get alert by /alerts/v1/search"
    responseObjs = json.loads(response.decode("utf-8"))
    assert responseObjs['totalRows'] > 0
    for obj in responseObjs['searchResults']:
        assert 'malware' in obj['metadata']
        assert obj['metadata']['objectExposure'] == "public"
        assert "bucketName" in obj['metadata']
        assert "dataIdentifiers" in obj['metadata']


@pytest.mark.API
@pytest.mark.parametrize('add_alerts', [payloads["alert_single_update"]], indirect=True)
def test_timestamp_update(add_alerts, testbed):
    '''
    :param testbed:
    test steps:
    1. create an alert;
    2. judge whether this alert has  lastUpdatedTs and lastOpenStateTs field, and not has lastStatusChangeTs
    3. do the single update to update the alert status from open to snooze;
    4. make sure this alert has  lastUpdatedTs, lastStatusChangeTs, lastOpenStateTs.
    5. do the single update to update the alert status from snooze to open
    6. make sure this alert has  lastUpdatedTs, lastStatusChangeTs, lastOpenStateTs.
    '''
    alerts_ids, customID = add_alerts
    assert len(alerts_ids) == 1
    alert_id = alerts_ids[0]
    client = HttpClient()
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    # check the alert detail by alert id
    status, response = get_alert_status(testbed['api_rest'], alert_id, headers=headers)
    assert status == requests.codes.ok
    responseObj = json.loads(response.decode("utf-8"))
    print(responseObj)

    assert "lastOpenStateTs" in responseObj, 'lastOpenStateTs is not exist'
    assert "lastUpdatedTs" in responseObj, 'lastUpdatedTs is not exist'
    assert 'lastStatusChangeTs' not in responseObj, 'lastStatusChangeTs is exist without status changing'
    last_opents = responseObj['lastOpenStateTs']
    last_updatets = responseObj['lastUpdatedTs']

    # do single update which snooze the alerts
    alerts_single_update_endpoint = "%s/alerts/v1/alertRecord/%s/status" % (
        testbed['api_rest'], alert_id)
    bulk_single_payloads = payloads["alert_single_update"]["alert_statues_single_update"]["open_to_snoozed"]
    status, response = client.rest_send_request(uri=alerts_single_update_endpoint, method='put', headers=headers,
                                                data=json.dumps(bulk_single_payloads))
    assert status == requests.codes.ok
    sleep(20)
    # check the alert detail by alert id
    status, response = get_alert_status(testbed['api_rest'], alert_id, headers=headers)
    assert status == requests.codes.ok
    responseObj = json.loads(response.decode("utf-8"))
    assert 'lastStatusChangeTs' in responseObj, 'lastStatusChangeTs is not exist'
    assert "lastOpenStateTs" in responseObj, 'lastOpenStateTs is not exist'
    assert "lastUpdatedTs" in responseObj, 'lastUpdatedTs is not exist'
    last_statusts = responseObj['lastStatusChangeTs']
    last_opents_2 = responseObj['lastOpenStateTs']
    last_updatets_2 = responseObj['lastUpdatedTs']
    assert last_statusts == last_updatets_2, 'lastUpdatedTs is not equal to lastStatusChangeTs after snoozed the alert'
    assert last_statusts > last_opents_2, 'lastStatusChangeTs is not bigger to lastOpenStateTs after snoozed the ' \
                                          'alert '
    assert last_opents_2 == last_opents, 'lastOpenStateTs is  not equal to previous lastOpenStateTs after snoozed the ' \
                                         'alert '
    assert last_updatets_2 > last_updatets, 'lastUpdatedTs  is not bigger to previous lastUpdatedTs after snoozed the ' \
                                            'alert '

    # do single update which reopen the alerts
    alerts_single_update_endpoint = "%s/alerts/v1/alertRecord/%s/status" % (
        testbed['api_rest'], alert_id)
    bulk_single_payloads = payloads["alert_single_update"]["alert_statues_single_update"]["snoozed_to_open"]
    status, response = client.rest_send_request(uri=alerts_single_update_endpoint, method='put', headers=headers,
                                                data=json.dumps(bulk_single_payloads))
    assert status == requests.codes.ok
    sleep(20)
    # check the alert detail by alert id
    status, response = get_alert_status(testbed['api_rest'], alert_id, headers=headers)
    assert status == requests.codes.ok
    responseObj = json.loads(response.decode("utf-8"))
    assert 'lastStatusChangeTs' in responseObj, 'lastStatusChangeTs is not exist'
    assert "lastOpenStateTs" in responseObj, 'lastOpenStateTs is not exist'
    assert "lastUpdatedTs" in responseObj, 'lastUpdatedTs is not exist'
    last_statusts_2 = responseObj['lastStatusChangeTs']
    last_opents_3 = responseObj['lastOpenStateTs']
    last_updatets_3 = responseObj['lastUpdatedTs']
    assert last_statusts_2 == last_updatets_3, 'lastUpdatedTs is not equal to lastStatusChangeTs after reopened the ' \
                                               'alert '
    assert last_statusts_2 == last_opents_3, 'lastStatusChangeTs is not equal to lastOpenStateTs after reopened the ' \
                                             'alert '
    assert last_opents_3 > last_opents_2, 'lastOpenStateTs is not bigger to previous lastOpenStateTs after reopened ' \
                                          'the alert '
    assert last_updatets_3 > last_updatets_2, 'lastUpdatedTs is not bigger previous lastUpdatedTs after reopened the ' \
                                              'alert '


@pytest.mark.API
@pytest.mark.parametrize('add_alerts', [payloads["alert_single_update"]], indirect=True)
def test_timestamp_search(add_alerts, testbed):
    '''
    :param testbed:
    test steps:
    1. create an alert;
    2. get the alert's detail including lastUpdatedTs and lastOpenStateTs
    3. search the alerts by the lastUpdatedTs and lastOpenStateTs filter, make sure the trigger alert is included
    4. do the single update to update the alert status from open to snooze;
    5. search the alerts by the lastUpdatedTs and lastOpenStateTs filter, make sure the trigger alert is included
    '''
    alerts_ids, customID = add_alerts
    assert len(alerts_ids) == 1
    alert_id = alerts_ids[0]
    client = HttpClient()
    headers = {'Content-Type': 'application/json', 'Authorization': testbed['auth_key']}
    # check the alert detail by alert id
    status, response = get_alert_status(testbed['api_rest'], alert_id, headers=headers)
    assert status == requests.codes.ok
    responseObj = json.loads(response.decode("utf-8"))
    print(responseObj)

    assert "lastOpenStateTs" in responseObj, 'lastOpenStateTs is not exist'
    assert "lastUpdatedTs" in responseObj, 'lastUpdatedTs is not exist'
    assert 'lastStatusChangeTs' not in responseObj, 'lastStatusChangeTs is exist without status changing'
    last_opents = responseObj['lastOpenStateTs']
    last_updatets = responseObj['lastUpdatedTs']

    # query by lastOpenStateTs
    alerts_search_endpoint = "%s/alerts/v1/search" % testbed['api_aggregator']
    search_payloads = payloads["alert_single_update"]["alerts_search"]
    search_payloads['customerId'] = customID
    search_payloads['filters']['timeRange']['forField'] = 'lastOpenStateTs'
    search_payloads['filters']['timeRange']['endTime'] = last_opents

    sleep(20)

    status, response = client.rest_send_request(uri=alerts_search_endpoint, method='post', headers=headers,
                                                data=json.dumps(search_payloads))
    assert status == requests.codes.ok, "error when get alert by /alerts/v1/search"
    responseObjs = json.loads(response.decode("utf-8"))
    alert_exist = False
    for obj in responseObjs['searchResults']:
        if alert_id == obj["alertId"]:
            alert_exist = True
            break
    assert alert_exist, "Search by lastOpenStateTs goes wrong"

    # query by lastUpdatedTs
    search_payloads['filters']['timeRange']['forField'] = 'lastUpdatedTs'
    search_payloads['filters']['timeRange']['endTime'] = last_updatets

    sleep(20)

    status, response = client.rest_send_request(uri=alerts_search_endpoint, method='post', headers=headers,
                                                data=json.dumps(search_payloads))
    assert status == requests.codes.ok, "error when get alert by /alerts/v1/search"
    responseObjs = json.loads(response.decode("utf-8"))
    alert_exist = False
    for obj in responseObjs['searchResults']:
        if alert_id == obj["alertId"]:
            alert_exist = True
            break
    assert alert_exist, "Search by lastUpdatedTs goes wrong"

    # do single update which snooze the alerts
    alerts_single_update_endpoint = "%s/alerts/v1/alertRecord/%s/status" % (
        testbed['api_rest'], alert_id)
    bulk_single_payloads = payloads["alert_single_update"]["alert_statues_single_update"]["open_to_snoozed"]
    status, response = client.rest_send_request(uri=alerts_single_update_endpoint, method='put', headers=headers,
                                                data=json.dumps(bulk_single_payloads))
    assert status == requests.codes.ok
    sleep(20)
    # check the alert detail by alert id
    status, response = get_alert_status(testbed['api_rest'], alert_id, headers=headers)
    assert status == requests.codes.ok
    responseObj = json.loads(response.decode("utf-8"))
    assert 'lastStatusChangeTs' in responseObj, 'lastStatusChangeTs is not exist'
    assert "lastOpenStateTs" in responseObj, 'lastOpenStateTs is not exist'
    assert "lastUpdatedTs" in responseObj, 'lastUpdatedTs is not exist'
    last_statusts = responseObj['lastStatusChangeTs']
    last_opents_2 = responseObj['lastOpenStateTs']
    last_updatets_2 = responseObj['lastUpdatedTs']

    # query by lastOpenStateTs
    alerts_search_endpoint = "%s/alerts/v1/search" % testbed['api_aggregator']
    search_payloads = payloads["alert_single_update"]["alerts_search"]
    search_payloads['customerId'] = customID
    search_payloads['filters']['timeRange']['forField'] = 'lastOpenStateTs'
    search_payloads['filters']['timeRange']['endTime'] = last_opents_2

    sleep(20)

    status, response = client.rest_send_request(uri=alerts_search_endpoint, method='post', headers=headers,
                                                data=json.dumps(search_payloads))
    assert status == requests.codes.ok, "error when get alert by /alerts/v1/search"
    responseObjs = json.loads(response.decode("utf-8"))
    alert_exist = False
    for obj in responseObjs['searchResults']:
        if alert_id == obj["alertId"]:
            alert_exist = True
            break
    assert alert_exist, "Search by lastOpenStateTs goes wrong"

    # query by lastUpdatedTs
    search_payloads['filters']['timeRange']['forField'] = 'lastUpdatedTs'
    search_payloads['filters']['timeRange']['endTime'] = last_updatets_2

    sleep(20)

    status, response = client.rest_send_request(uri=alerts_search_endpoint, method='post', headers=headers,
                                                data=json.dumps(search_payloads))
    assert status == requests.codes.ok, "error when get alert by /alerts/v1/search"
    responseObjs = json.loads(response.decode("utf-8"))
    alert_exist = False
    for obj in responseObjs['searchResults']:
        if alert_id == obj["alertId"]:
            alert_exist = True
            break
    assert alert_exist, "Search by lastUpdatedTs goes wrong"

    # query by lastStatusChangeTs
    search_payloads['filters']['timeRange']['forField'] = 'lastStatusChangeTs'
    search_payloads['filters']['timeRange']['endTime'] = last_statusts

    sleep(20)

    status, response = client.rest_send_request(uri=alerts_search_endpoint, method='post', headers=headers,
                                                data=json.dumps(search_payloads))
    assert status == requests.codes.ok, "error when get alert by /alerts/v1/search"
    responseObjs = json.loads(response.decode("utf-8"))
    alert_exist = False
    for obj in responseObjs['searchResults']:
        if alert_id == obj["alertId"]:
            alert_exist = True
            break
    assert alert_exist, "Search by lastStatusChangeTs goes wrong"
