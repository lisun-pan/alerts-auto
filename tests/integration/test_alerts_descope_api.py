import json
import os
from time import sleep

import pytest
import requests

from libs.utils.http_client import HttpClient
from libs.utils.logger import get_logger
from libs.utils.pytest_utils import get_test_data_file_content
from tests.integration.conftest import get_alert_status

payloads = get_test_data_file_content('data/api_tests.json')
logger = get_logger(os.path.basename(__file__).replace(".py", ""))


def get_test_names(test_kind, test_data):
    """
     Method to return the list of tests reading the test data file content
    """
    test_names = []
    for test_name in list(payloads[test_kind].keys()):
        if test_name == test_data:
            for tests in list(payloads[test_kind][test_name].keys()):
                test_names.append(pytest.param(tests, id=tests))

    return test_names


@pytest.mark.parametrize('add_alerts', [payloads["alert_descope"]], indirect=True)
@pytest.mark.parametrize('test_name', get_test_names('alert_descope', 'alert_descope_tests'))
def test_descope_api(add_alerts, test_name, testbed):
    """
        Test Steps:
        1. post alerts to /alerts;
        2. get the alert ids which are new generated
        2. check the alerts status which should be open;
        3. post payload to descope api
        4. check the alerts status which should be resolved;
    """
    alerts_ids = add_alerts
    client = HttpClient()
    alerts_descope_endpoint = "%s/alerts/v1/descope" % testbed['alerts_api']
    descope_payloads = payloads["alert_descope"]["alert_descope_tests"][test_name]
    headers = {'Authorization': testbed['auth_key']}
    status, response = client.rest_send_request(uri=alerts_descope_endpoint, method='put', headers=headers,
                                                data=json.dumps(descope_payloads))
    assert status == requests.codes.accepted, descope_payloads
    sleep(30)
    # check the alerts status which should be dismissed
    for alert_id in alerts_ids:
        status, response = get_alert_status(testbed['alerts_server'], alert_id, headers=headers)
        assert status == requests.codes.ok
        responseObj = json.loads(response.decode("utf-8"))
        if responseObj["customerId"] == descope_payloads["customerId"]:
            assert responseObj["alertStatus"] == 5
