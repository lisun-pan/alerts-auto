import os
import json
import time
import pytest
import requests

from libs.utils.http_client import HttpClient
from libs.utils.logger import get_logger
from libs.utils.pytest_utils import get_test_data_file_content
from libs.utils.elasticsearch_util import ElasticSearchDB
from libs.utils.kafka_client import KafkaClient


payloads = get_test_data_file_content('data/alert_tests.json')
logger = get_logger(os.path.basename(__file__).replace(".py", ""))


def get_test_names(test_data):
    """
     Method to return the list of tests reading the test data file content
    """
    test_names = []
    print("Entering get_test_names...")
    for test_name in list(payloads.keys()):
        print("testname:", test_name)
        print("input:", test_data)
        if test_name == test_data:
            for tests in list(payloads[test_name].keys()):
                print("tests:", tests)
                test_names.append(pytest.param(tests, id=tests))
                print("test names:", test_names)

    return test_names


'''
@pytest.mark.parametrize('test_name', get_test_names('alert_raw_main'))
def post_alerts(test_name, testbed):
    # Consuming from a Kafka topic

    payload = payloads["alert_raw_main"][test_name]
    alerts_endpoint = "%s:9081/enqueuer/alerts" % testbed['alerts_server']
    #for each_payload in payload:

    print("Running Tests....post_alerts")
    print("HTTP POST to the Enqueuer service")

    # HTTP POST to the Alert Enqueuer Service
    # test_payload = [each_payload]
    test_payload = payload
    print(test_payload)
    client = HttpClient()
    status, response = client.rest_send_request(uri=alerts_endpoint, method='post',
                                                data=json.dumps(test_payload))
    logger.info(response)
    print("Response:", status)
    assert status == requests.codes.ok
'''

@pytest.mark.FUNCTIONAL
@pytest.mark.parametrize('test_name', get_test_names('alert_indexer_main'))
def test_index_writer(test_name, testbed):

    # Verification of the Indexer
    payload = payloads["alert_search_main"][test_name]

    elastic_search = "elastic:elastic@%s/local-alerts/_search" % testbed['elasticsearch']
    for each_payload in payload:
        print("Running Tests....Indexer service")
        print("each_payload:", each_payload)

        client = HttpClient()
        # Produce Alert message to a Kafka topic
        kafkaClient = KafkaClient(kafka_servers=testbed['kafka_servers'])
        kafkaClient.write_topic(testbed['notification_topic'], each_payload)
        time.sleep(5)

        # Connect to the Elastic Search and verify the records in the DB
        print("__________________________________________________")
        print("Verifying the records in Elastic Search DB ...")
        ElasticSearchObj = ElasticSearchDB()
        res = ElasticSearchObj.connect_to_es()

        # data = json.dumps(search_payload))
        status, response = client.rest_send_request(uri=elastic_search, method='GET')

        print("Response Code:", status)
        print("Response:", response)
        logger.info("response payload:", response)
        response = json.loads(response.decode("utf-8"))
        print("total hits:", response['hits']['total'])

        # print(res.search(index="alerts", body={"customerId": "1112"}))
        # print(res.search())



@pytest.mark.INTEGRATION
@pytest.mark.parametrize('test_name', get_test_names('alert_indexer_main'))
def test_indexer(test_name, testbed):


    # Bulk post the data to the REST endpoint
    payload = payloads["alert_raw_main"]["alert_bulk"]
    alerts_endpoint = "%s:9081/alerts/v1/policyVerdict" % testbed['alerts_server']
    # for each_payload in payload:

    print("Running Tests....post_alerts")
    print("HTTP POST to the REST service")

    # HTTP POST to the Alert Enqueuer Service
    # test_payload = [each_payload]
    test_payload = payload
    print(test_payload)
    client = HttpClient()
    status, response = client.rest_send_request(uri=alerts_endpoint, method='post',
                                                data=json.dumps(test_payload))
    logger.info(response)
    print("Response:", status)
    assert status == 202


    # Verification of the Indexer
    payload = payloads["alert_search_main"][test_name]
    print("payload:", payload)
    elastic_search = "elastic:elastic@%s/local-alerts/_search" % testbed['elasticsearch']
    for each_payload in payload:
        print("Running Tests....Indexer service")
        print("each_payload:", each_payload)

        client = HttpClient()

        time.sleep(10)
        # Connect to the Elastic Search and verify the records in the DB
        print("__________________________________________________")
        print("Verifying the records in Elastic Search DB ...")
        ElasticSearchObj = ElasticSearchDB()
        res = ElasticSearchObj.connect_to_es()

        # data = json.dumps(search_payload))
        status, response = client.rest_send_request(uri=elastic_search, method='GET')

        print("Response Code:", status)
        print("Response:", response)
        logger.info("response payload:", response)
        response = json.loads(response.decode("utf-8"))
        print("total hits:", response['hits']['total'])



@pytest.mark.INTEGRATION
@pytest.mark.parametrize('test_name', get_test_names('alert_search_main'))
def test_search(test_name, testbed):

    # Bulk post the data to the REST endpoint
    payload = payloads["alert_raw_main"]["alert_bulk"]
    alerts_endpoint = "%s:9081/alerts/v1/policyVerdict" % testbed['alerts_server']
    # for each_payload in payload:

    print("Running Tests....post_alerts")
    print("HTTP POST to the Enqueuer service")

    # HTTP POST to the Alert Enqueuer Service
    # test_payload = [each_payload]
    test_payload = payload
    print(test_payload)
    client = HttpClient()
    status, response = client.rest_send_request(uri=alerts_endpoint, method='post',
                                                data=json.dumps(test_payload))
    logger.info(response)
    print("Response:", status)
    assert status == 202


    # Verification of the Search tests

    payload = payloads["alert_search_main"][test_name]
    print("Running Tests....Search Service")
    alerts_search = "%s:9084/alerts/v1/search" % testbed['alerts_server']
    # HTTP POST to the Alert Search/Aggregation Service
    # print(test_payload)
    client = HttpClient()

    for each_payload in payload:

        status, response = client.rest_send_request(uri=alerts_search, method='POST',
                                                    data=json.dumps(each_payload))
        logger.info(response)
        assert status == requests.codes.ok

        print("Response Status:", status)
        responseObj = json.loads(response.decode("utf-8"))
        print("response payload:", responseObj)
        logger.info("response payload:", responseObj)
        assert len(responseObj) != 0
        for each_resp_obj in responseObj:
            print(each_resp_obj)
            assert len(each_resp_obj) != 0

@pytest.mark.INTEGRATION
@pytest.mark.parametrize('test_name', get_test_names('alert_aggregator_main'))
def test_aggregator(test_name, testbed):

    # Bulk post the data to the REST endpoint
    payload = payloads["alert_raw_main"]["alert_bulk"]
    alerts_endpoint = "%s:9081/alerts/v1/policyVerdict" % testbed['alerts_server']
    # for each_payload in payload:

    print("Running Tests....post_alerts")
    print("HTTP POST to the Enqueuer service")

    # HTTP POST to the Alert Enqueuer Service
    # test_payload = [each_payload]
    test_payload = payload
    print(test_payload)
    client = HttpClient()
    status, response = client.rest_send_request(uri=alerts_endpoint, method='post',
                                                data=json.dumps(test_payload))
    logger.info(response)
    print("Response:", status)
    assert status == 202


    # Verification of the aggregator service

    payload = payloads["alert_aggregator_main"][test_name]
    alerts_search = "%s:9084/alerts/v1/aggregate" % testbed['alerts_server']
    print("Running Tests....Aggregator Service")

    # HTTP POST to the Alert Search/Aggregation Service
    # print(test_payload)
    client = HttpClient()

    for each_payload in payload:
        print("each_payload:", each_payload)
        status, response = client.rest_send_request(uri=alerts_search, method='POST',
                                                    data=json.dumps(each_payload))
        logger.info(response)
        assert status == requests.codes.ok
        print("Response Status:", status)
        responseObj = json.loads(response.decode("utf-8"))
        print("response payload:", responseObj)
        logger.info("response payload:", responseObj)
        assert len(responseObj) != 0
        for each_resp_obj in responseObj:
            assert len(each_resp_obj) != 0


@pytest.mark.INTEGRATION
@pytest.mark.parametrize('test_name', get_test_names('alert_bulk_update_main'))
def test_bulk_worker(test_name, testbed):
    print("Entering the bulk worker...")
    # Bulk post the data to the REST endpoint
    payload = payloads["alert_raw_main"]["alert_bulk"]
    alerts_endpoint = "%s:9081/alerts" % testbed['alerts_server']
    # for each_payload in payload:

    print("Running Tests....post_alerts")
    print("HTTP POST to the Enqueuer service")

    # HTTP PUT to the Alert Enqueuer Service
    # test_payload = [each_payload]
    test_payload = payload
    print(test_payload)
    client = HttpClient()
    status, response = client.rest_send_request(uri=alerts_endpoint, method='put',
                                                data=json.dumps(test_payload))
    logger.info(response)
    print("Response:", status)
    assert status == requests.codes.ok

    # Verification of the bulk worker service

    payload = payloads["alert_bulk_main"][test_name]
    alerts_search = "%s:9084/alerts/status" % testbed['alerts_server']
    print("Running Tests....Bulk worker Service")

    # HTTP PUT to the Alert bulk worker Service
    # print(test_payload)
    client = HttpClient()

    for each_payload in payload:
        print("each_payload:", each_payload)
        status, response = client.rest_send_request(uri=alerts_search, method='PUT',
                                                    data=json.dumps(each_payload))
        logger.info(response)
        assert status == requests.codes.ok
        print("Response Status:", status)
        responseObj = json.loads(response.decode("utf-8"))
        print("response payload:", responseObj)
        logger.info("response payload:", responseObj)
        assert len(responseObj) != 0
        for each_resp_obj in responseObj:
            assert len(each_resp_obj) != 0

