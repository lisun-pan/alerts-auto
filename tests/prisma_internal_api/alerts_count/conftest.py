import pytest
import json
from libs.utils.pytest_utils import get_test_data_file_content


@pytest.fixture(scope='class')
def create_alerts(testbed, public_api_fixture):
    payloads = get_test_data_file_content('data/alert_count.json')
    alerts_create_data = payloads['alert_create_for_count_data']['create_alerts']
    status, response = public_api_fixture.rest_send_request_with_token('alerts/v1/policyVerdict', 'post',
                                                                       testbed['auth_key'],
                                                                       data=json.dumps(alerts_create_data),
                                                                       timeout=120)
    print(response)

    if status == 202:
        print("Alerts created successfully")
    else:
        print("Failed to create alerts")