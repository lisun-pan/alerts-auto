import pytest
import json
import datetime
import os
import calendar
import random
import time

from pytz import timezone
from libs.utils.pytest_utils import get_test_data_file_content
from libs.utils.logger import get_logger


payloads = get_test_data_file_content('data/alert_count.json')
logger = get_logger(os.path.basename(__file__).replace(".py", ""))


def get_current_epoch():
    """
    Gets the current PST time and convert to current epoch time

    :return: current epoch time
    """
    current_time = datetime.datetime.utcnow()
    current_time_pacific = current_time.astimezone(timezone('US/Pacific'))
    cur_epoch = calendar.timegm(current_time_pacific.timetuple()) * 1000
    return cur_epoch


def send_request(api, method, rest_session, auth_key, payload):
    """
    Send alerts api requests to redlock platform

    :param api: uri
    :param method: type of api call
    :param rest_session: Its a fixture, that  ,
    :param auth_key: long liv token
    :param payload:
    :return: response
    """
    status, response = rest_session.rest_send_request_with_token(api, method, auth_key,
                                                                 data=json.dumps(payload),
                                                                 timeout=120)
    print("Status and response are: {}, {}".format(status, response))
    if status == 200:
        response = json.loads(response)
        return response
    return None


@pytest.mark.API
@pytest.mark.usefixtures("create_alerts")
class TestAlertsCountForDifferentStatus(object):
    def test_alerts_open_counts(self, request, testbed, public_api_fixture):
        """
        This test checks the alert open count. Create alerts,
        call search api and store the alert ids. Put call
        to change the single status of alert. Post call to the
        alert count api and verify the count of open alerts which
        matches the total rows provided by search call

        :param request: pytest request object
        :param testbed: long live token
        :param public_api_fixture: public api rest session
        """
        testname = request.node.name
        count_payload = payloads['count_alert_data'][testname]
        search_payload = payloads['alert_search_data']['search_alerts']
        current_time = datetime.datetime.utcnow()
        current_time_pacific = current_time.astimezone(timezone('US/Pacific'))
        cur_epoch = calendar.timegm(current_time_pacific.timetuple()) * 1000

        search_payload['filters']['timeRange']['endTime'] = cur_epoch
        count_payload['filters']['timeRange']['endTime'] = cur_epoch
        print("Search payload is: {}".format(search_payload))
        print("Count payload is: {}".format(count_payload))
        status, response = public_api_fixture.rest_send_request_with_token('alerts/v1/search', 'post',
                                                                           testbed['auth_key'],
                                                                           data=json.dumps(search_payload),
                                                                           timeout=120)
        if status == 200:
            response = json.loads(response)
            print(response)
            num_of_alerts = response['totalRows']

        status, response = public_api_fixture.rest_send_request_with_token('alerts-internal/v1/count', 'post',
                                                                           testbed['auth_key'],
                                                                           data=json.dumps(count_payload),
                                                                           timeout=120)
        if status == 200:
            response = json.loads(response)
            print(response)

        assert num_of_alerts == response['count']

    def test_alerts_dismiss_counts(self, testbed, public_api_fixture):
        """
        This test checks the alerts dismiss count.
        Call search api and store the alert ids. Put call
        to change the single status of alert, Post
        call to the alert count api and verify the count of
        dismiss alerts and open count alerts
        """
        search_payload = payloads['alert_search_data']['search_alerts']
        single_status_update_payload = payloads['single_status_update']['dismiss_alert']
        single_status_open_payload = payloads['single_status_update']['open_alert']
        count_payload = payloads['count_alert_data']['test_alerts_open_counts']

        search_payload['filters']['timeRange']['endTime'] = get_current_epoch()
        count_payload['filters']['timeRange']['endTime'] = get_current_epoch()

        response = send_request('alerts/v1/search', 'post', public_api_fixture, testbed['auth_key'], search_payload)
        if response:
            num_alerts_match = response['totalRows']
            print("Total rows of search match are {}".format(num_alerts_match))
            alert_ids = list()
            alert_status = list()
            for item in response['searchResults']:
                alert_ids.append(item['alertId'])
                alert_status.append(item['alertStatus'])

            print("Alert ids and alert status are : {} and {}".format(alert_ids, alert_status))

        one_alert_id = random.choice(alert_ids)
        print("Alert id selected: {}".format(one_alert_id))

        response = send_request('alerts/v1/alertRecord/{}/status'.format(one_alert_id), 'put', public_api_fixture,
                                testbed['auth_key'],
                                single_status_update_payload)
        if response:
            print("Alert id change status is {}".format(response))

            t_end = time.time() + 8
            while time.time() < t_end:
                count_payload['filters']['timeRange']['endTime'] = get_current_epoch()
                count_payload['filters']['status'] = ["1"]
                print("Count payload is {}".format(count_payload))

                response = send_request('alerts-internal/v1/count', 'post', public_api_fixture, testbed['auth_key'],
                                         count_payload)

                if response:
                    open_alerts_count = response['count']
                    print("Open alerts count is {}".format(response))

                count_payload['filters']['status'] = ["2"]
                print("Count payload is {}".format(count_payload))

                response = send_request('alerts-internal/v1/count', 'post', public_api_fixture, testbed['auth_key'],
                                         count_payload)

                if response:
                    dismiss_alerts_count = response['count']
                    print("Dismiss alert count is {}".format(response))

                if dismiss_alerts_count == 1 and open_alerts_count == 3:
                    break

            response = send_request('alerts/v1/alertRecord/{}/status'.format(one_alert_id), 'put', public_api_fixture,
                                    testbed['auth_key'], single_status_open_payload)

            if response:
                print("Alert with id {} is now reopened".format(one_alert_id))

        assert open_alerts_count == 3
        assert dismiss_alerts_count == 1
        assert num_alerts_match == open_alerts_count + dismiss_alerts_count

    def test_alerts_snoozed_counts(self, public_api_fixture, testbed):
        """
        This test checks the alerts snoozed count. Call search
        api and store the alert ids. Put call for one alert id
        to change the single status , Post call to the alert
        count api and verify the count of snoozed alerts and open count alerts
        """

        search_payload = payloads['alert_search_data']['search_alerts']
        single_status_update_payload = payloads['single_status_update']['snoozed_alert']
        count_payload = payloads['count_alert_data']['test_alerts_open_counts']
        single_status_open_payload = payloads['single_status_update']['open_alert']

        search_payload['filters']['timeRange']['endTime'] = get_current_epoch()

        response = send_request('alerts/v1/search', 'post', public_api_fixture, testbed['auth_key'], search_payload)

        if response:
            num_alerts_match = response['totalRows']
            print("Total rows of search match are {}".format(num_alerts_match))
            alert_ids = list()
            alert_status = list()
            for item in response['searchResults']:
                alert_ids.append(item['alertId'])
                alert_status.append(item['alertStatus'])

            print("Alert ids and alert status are : {} and {}".format(alert_ids, alert_status))

        one_alert_id = random.choice(alert_ids)
        print("Alert id selected: {}".format(one_alert_id))

        response = send_request('alerts/v1/alertRecord/{}/status'.format(one_alert_id), 'put', public_api_fixture,
                                testbed['auth_key'], single_status_update_payload)
        if response:
            print("Alert id change status is {}".format(response))

            t_end = time.time() + 8
            while time.time() < t_end:
                count_payload['filters']['timeRange']['endTime'] = get_current_epoch()
                count_payload['filters']['status'] = ["1"]
                print("Count payload is {}".format(count_payload))

                response = send_request('alerts-internal/v1/count', 'post', public_api_fixture, testbed['auth_key'],
                                        count_payload)
                if response:
                    open_alerts_count = response['count']
                    print("Open alerts count is {}".format(response))

                count_payload['filters']['status'] = ["3"]
                print("Count payload is {}".format(count_payload))

                response = send_request('alerts-internal/v1/count', 'post', public_api_fixture, testbed['auth_key'],
                                        count_payload)
                if response:
                    snoozed_alerts_count = response['count']
                    print("Snooze alert count is {}".format(response))

                if snoozed_alerts_count == 1 and open_alerts_count == 3:
                    break

            response = send_request('alerts/v1/alertRecord/{}/status'.format(one_alert_id), 'put', public_api_fixture,
                                    testbed['auth_key'], single_status_open_payload)

            if response:
                print("Alert with id {} is now reopened".format(one_alert_id))

        assert open_alerts_count == 3
        assert snoozed_alerts_count == 1
        assert num_alerts_match == open_alerts_count + snoozed_alerts_count

    def test_open_to_resolved(self, testbed, public_api_fixture):
        """
        This test checks the alerts resolved count. Call search
        api and store the alert ids. Put call for one alert id
        to change the single status , Post call to the alert
        count api and verify the count of resolved alerts and open count alerts
        """

        search_payload = payloads['alert_search_data']['search_alerts']
        single_status_update_payload = payloads['single_status_update']['resolved_alert']
        count_payload = payloads['count_alert_data']['test_alerts_open_counts']
        single_status_open_payload = payloads['single_status_update']['open_alert']

        search_payload['filters']['timeRange']['endTime'] = get_current_epoch()

        response = send_request('alerts/v1/search', 'post', public_api_fixture, testbed['auth_key'], search_payload)

        if response:
            num_alerts_match = response['totalRows']
            print("Total rows of search match are {}".format(num_alerts_match))
            alert_ids = list()
            alert_status = list()
            for item in response['searchResults']:
                alert_ids.append(item['alertId'])
                alert_status.append(item['alertStatus'])

            print("Alert ids and alert status are : {} and {}".format(alert_ids, alert_status))

        one_alert_id = random.choice(alert_ids)
        print("Alert id selected: {}".format(one_alert_id))

        response = send_request('alerts/v1/alertRecord/{}/status'.format(one_alert_id), 'put', public_api_fixture,
                                testbed['auth_key'], single_status_update_payload)
        if response:
            print("Alert id change status is {}".format(response))

            t_end = time.time() + 8
            while time.time() < t_end:
                count_payload['filters']['timeRange']['endTime'] = get_current_epoch()
                count_payload['filters']['status'] = ["1"]
                print("Count payload is {}".format(count_payload))

                response = send_request('alerts-internal/v1/count', 'post', public_api_fixture, testbed['auth_key'],
                                         count_payload)
                if response:
                    open_alerts_count = response['count']
                    print("Open alerts count is {}".format(response))

                count_payload['filters']['status'] = ["3"]
                print("Count payload is {}".format(count_payload))

                response = send_request('alerts-internal/v1/count', 'post', public_api_fixture, testbed['auth_key'],
                                        count_payload)
                if response:
                    resolved_alerts_count = response['count']
                    print("Resolved alert count is {}".format(response))

                if resolved_alerts_count == 1 and open_alerts_count == 3:
                    break

            response = send_request('alerts/v1/alertRecord/{}/status'.format(one_alert_id), 'put', public_api_fixture,
                                    testbed['auth_key'], single_status_open_payload)

            if response:
                print("Alert with id {} is now reopened".format(one_alert_id))

        assert open_alerts_count == 3
        assert resolved_alerts_count == 1
        assert num_alerts_match == open_alerts_count + resolved_alerts_count

    def test_open_to_snoozed_to_resolved(self, testbed, public_api_fixture):
        """
        This test checks the alerts resolved, snoozed and
        open count. Call search api and store the alert ids. Put
        call for one alert id to change the single status,
        Post call to the alert count api and verify
        the count of resolved alerts and open count alerts
        """
        search_payload = payloads['alert_search_data']['search_alerts']
        search_payload['filters']['timeRange']['endTime'] = get_current_epoch()
        single_status_update_to_snoozed_payload = payloads['single_status_update']['snoozed_alert']
        single_status_update_to_resolved_payload = payloads['single_status_update']['resolved_alert']
        single_status_open_payload = payloads['single_status_update']['open_alert']
        count_payload = payloads['count_alert_data']['test_alerts_open_counts']

        response = send_request('alerts/v1/search', 'post', public_api_fixture, testbed['auth_key'],
                                        search_payload)
        if response:
            num_alerts_match = response['totalRows']
            print("Total rows of search match are {}".format(num_alerts_match))
            alert_ids = list()
            alert_status = list()
            for item in response['searchResults']:
                alert_ids.append(item['alertId'])
                alert_status.append(item['alertStatus'])

            print("Alert ids and alert status are : {} and {}".format(alert_ids, alert_status))

        one_alert_id = random.choice(alert_ids)
        print("Alert id selected: {}".format(one_alert_id))

        response = send_request('alerts/v1/alertRecord/{}/status'.format(one_alert_id), 'put', public_api_fixture,
                                testbed['auth_key'], single_status_update_to_snoozed_payload)
        if response:
            print("Alert id change status is {}".format(response))

            t_end = time.time() + 8
            while time.time() < t_end:
                count_payload['filters']['timeRange']['endTime'] = get_current_epoch()
                count_payload['filters']['status'] = ["1"]
                print("Count payload is {}".format(count_payload))

                response = send_request('alerts-internal/v1/count', 'post', public_api_fixture, testbed['auth_key'],
                                        count_payload)
                if response:
                    open_alerts_count = response['count']
                    print("Open alerts count is {}".format(response))

                count_payload['filters']['status'] = ["3"]
                print("Count payload is {}".format(count_payload))

                response = send_request('alerts-internal/v1/count', 'post', public_api_fixture, testbed['auth_key'],
                                        count_payload)
                if response:
                    snoozed_alerts_count = response['count']
                    print("Snoozed alert count is {}".format(response))

                if snoozed_alerts_count == 1 and open_alerts_count == 3:
                    break

        response = send_request('alerts/v1/alertRecord/{}/status'.format(one_alert_id), 'put', public_api_fixture,
                                testbed['auth_key'], single_status_update_to_resolved_payload)
        if response:
            print("Alert id change status is {}".format(response))

            t_end = time.time() + 8
            while time.time() < t_end:
                count_payload['filters']['timeRange']['endTime'] = get_current_epoch()
                count_payload['filters']['status'] = ["5"]
                print("Count payload is {}".format(count_payload))

                response = send_request('alerts-internal/v1/count', 'post', public_api_fixture, testbed['auth_key'],
                                        count_payload)
                if response:
                    resolved_alerts_count = response['count']
                    print("Resolved alerts count is {}".format(response))

                count_payload['filters']['status'] = ["3"]
                print("Count payload is {}".format(count_payload))

                response = send_request('alerts-internal/v1/count', 'post', public_api_fixture, testbed['auth_key'],
                                        count_payload)
                if response:
                    snoozed_alerts_count = response['count']
                    print("Snoozed alert count is {}".format(response))

                if snoozed_alerts_count == 0 and resolved_alerts_count == 1:
                    break

            response = send_request('alerts/v1/alertRecord/{}/status'.format(one_alert_id), 'put', public_api_fixture,
                                        testbed['auth_key'], single_status_open_payload)

            if response:
                print("Alert with id {} is now reopened".format(one_alert_id))

            assert open_alerts_count == 3
            assert snoozed_alerts_count == 0
            assert resolved_alerts_count == 1
            assert num_alerts_match == open_alerts_count + resolved_alerts_count

    def test_negative_faulty_payload_with_null_status(self, request, testbed, public_api_fixture):
        """
        This test checks the response with null status payload
        """
        out_flag = False
        testname = request.node.name
        count_payload = payloads['count_alert_data'][testname]

        status, response = public_api_fixture.rest_send_request_with_token('alerts-internal/v1/count', 'post',
                                                                           testbed['auth_key'],
                                                                           data=json.dumps(count_payload), timeout=120)
        print(status)

        if status == 400:
            print("Alert count are passed as null and status is {}".format(status))
            out_flag = True

        assert out_flag