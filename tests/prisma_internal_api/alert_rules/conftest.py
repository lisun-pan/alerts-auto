import pytest
import json
import os
from libs.utils.logger import get_logger


logger = get_logger(os.path.basename(__file__).replace(".py", ""))

#####################
## FIXTURES
#####################


@pytest.fixture(scope='class')
def get_cloud_accounts(request, public_api_fixture):
    status, response = public_api_fixture.rest_send_request('cloud', 'get', timeout=120)
    print("Status {}".format(status))
    if status == 200:
        response = json.loads(response)
        request.config.cloud_accounts = list()
        for item in response:
            request.config.cloud_accounts.append(item['accountId'])
    yield request.config.cloud_accounts


@pytest.fixture(scope='class')
def get_policies(request, public_api_fixture):
    status, response = public_api_fixture.rest_send_request('v2/policy', 'get', timeout=120)
    if status == 200:
        response = json.loads(response)
        request.config.get_policies = list()
        for item in response:
            request.config.get_policies.append(item['policyId'])


@pytest.fixture(scope='class')
def create_account_groups(request, public_api_fixture, get_cloud_accounts):
    payload = {'name': 'automation_test', 'accountIds': get_cloud_accounts}
    status, response = public_api_fixture.rest_send_request('cloud/group', 'post',
                                                          data=json.dumps(payload),
                                                          timeout=120)
    if status == 200:
        status, response = public_api_fixture.rest_send_request('cloud/group', 'get', timeout=120)
        response = json.loads(response)
        if status == 200:
            for item in response:
                if item['name'] == 'automation_test':
                    request.config.account_group_id = item['id']
                    break
        else:
            logger.error("Unable to fetch account groups")
    else:
        logger.error("Unable to create account group")

    yield

    edit_account_group_payload = {"name": "automation_test", "accountIds": []}
    status, response = public_api_fixture.rest_send_request('cloud/group/{}'.format(request.config.account_group_id),
                                                            'put', data=json.dumps(edit_account_group_payload),
                                                            timeout=120)
    if status == 200:
        logger.info("Account group edited successfully")
        status, response = public_api_fixture.rest_send_request('cloud/group/{}'.format(request.config.account_group_id),
                                                              'delete', timeout=120)
        print("Delete acc group st is {}".format(status))
        if status == 200:
            logger.info("account group deleted successfully")
        else:
            logger.info("Unable to delete account group")
    else:
        logger.info("Unable to edit account group")


#####################
## METHODS
#####################


def check_alert_rule_match(status, response, flag, alert_rule_name):
    if status == 200:
        response = json.loads(response)
        print(response)
        for i in response:
            for j in i['alertRuleDetails']:
                if j['name'] == alert_rule_name:
                    logger.info("Found the alert rule in match api")
                    flag = True
    return flag