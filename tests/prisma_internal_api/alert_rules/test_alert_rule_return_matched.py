import pytest
import json
import os
import random

from libs.utils.logger import get_logger
from libs.utils.pytest_utils import get_test_data_file_content
from tests.prisma_internal_api.alert_rules.conftest import check_alert_rule_match


payloads = get_test_data_file_content('data/alert_rule_match_data.json')
logger = get_logger(os.path.basename(__file__).replace(".py", ""))


@pytest.mark.API
@pytest.mark.usefixtures('get_cloud_accounts', 'get_policies', 'create_account_groups')
class TestAlertRuleMatched(object):
    def test_1_acc_all_pol_all_reg_no_tags(self, request, testbed, public_api_fixture):
        """
        Create payload with one accountIDs,
        ALL PolicyIDs, no tags,
        ALL regions and verify the response

        :param request: request fixture of pytest
        :param public_api_fixture: fixture returning
        publicapi session used for sending request
        :param testbed: fixture containing command
        line options

        :return:
        """
        out_flag = False
        testname = request.node.name

        alert_data = payloads['alert_rule_create_data'][testname]
        alert_rule_match_data = payloads['alert_rule_match_data'][testname]

        alert_data['target']['accountGroups'].append(request.config.account_group_id)
        alert_rule_match_data[0]['customerId'] = str(testbed['customerId'])
        alert_rule_match_data[0]['accountId'] = random.choice(request.config.cloud_accounts)

        status, response = public_api_fixture.rest_send_request('alert/rule', 'post', data=json.dumps(alert_data),
                                                                timeout=120)
        if status == 200:
            alert_rule_id = json.loads(response)['policyScanConfigId']
            print(alert_rule_id)

        status, response = public_api_fixture.rest_send_request_with_token('prisma-cloud-alert-rules/v1/alert-rule/match',
                                                                           'post',
                                                                           testbed['auth_key'],
                                                                           data=json.dumps(alert_rule_match_data))
        print("Alert rule match status is {}".format(status))
        out_flag = check_alert_rule_match(status, response, out_flag, alert_data['name'])

        status, response = public_api_fixture.rest_send_request('alert/rule/{}'.format(alert_rule_id),
                                                                'delete', timeout=120)
        print("Delete status is {}".format(status))
        logger.info("Delete status is {}".format(status))
        assert out_flag

    def test_1_acc_1_pol_ALL_reg_1_tags(self, request, testbed, public_api_fixture):
        """
        Create payload with one accountIDs,
        one PolicyIDs, one tags,
        all regions and verify the response

        :param request: request fixture of pytest
        :param testbed: testbed fixture from main conftest
        :param public_api_fixture: fixture returning
        publicapi session used for sending request

        :return:
        """
        out_flag = False
        pol_id = random.choice(request.config.get_policies)

        testname = request.node.name

        alert_data = payloads['alert_rule_create_data'][testname]
        alert_rule_match_data = payloads['alert_rule_match_data'][testname]

        alert_data['target']['accountGroups'].append(request.config.account_group_id)

        alert_rule_match_data[0]['customerId'] = str(testbed['customerId'])
        alert_rule_match_data[0]['accountId'] = random.choice(request.config.cloud_accounts)
        alert_rule_match_data[0]['policyId'] = pol_id

        print(pol_id)

        status, response = public_api_fixture.rest_send_request('alert/rule', 'post', data=json.dumps(alert_data), timeout=120)
        if status == 200:
            alert_rule_id = json.loads(response)['policyScanConfigId']
            print(alert_rule_id)

        print(alert_rule_match_data)
        status, response = public_api_fixture.rest_send_request_with_token('prisma-cloud-alert-rules/v1/alert-rule/match',
                                                                           'post', testbed['auth_key'],
                                                                           data=json.dumps(alert_rule_match_data))
        print("Alert rule match status is {}".format(status))
        out_flag = check_alert_rule_match(status, response, out_flag, alert_data['name'])

        status, response = public_api_fixture.rest_send_request('alert/rule/{}'.format(alert_rule_id),
                                                                'delete', timeout=120)
        print("Delete status is {}".format(status))

        assert out_flag

    def test_1_acc_1_pol_all_reg_no_tags(self, request, testbed, public_api_fixture):
        """
        Create payload with one accountIDs,
        one PolicyIDs, no tags,
        all regions and verify the response

        :param request: request fixture of pytest
        :param testbed: testbed fixture from main conftest
        :param public_api_fixture: fixture returning
        publicapi session used for sending request

        :return:
        """
        out_flag = False
        pol_id = random.choice(request.config.get_policies)
        testname = request.node.name

        alert_data = payloads['alert_rule_create_data'][testname]
        alert_rule_match_data = payloads['alert_rule_match_data'][testname]

        alert_data['target']['accountGroups'].append(request.config.account_group_id)
        alert_data['policies'].append(pol_id)

        alert_rule_match_data[0]['customerId'] = str(testbed['customerId'])
        alert_rule_match_data[0]['accountId'] = random.choice(request.config.cloud_accounts)
        alert_rule_match_data[0]['policyId'] = pol_id

        status, response = public_api_fixture.rest_send_request('alert/rule', 'post', data=json.dumps(alert_data),
                                                                timeout=120)
        if status == 200:
            alert_rule_id = json.loads(response)['policyScanConfigId']
            print(alert_rule_id)

        print(alert_rule_match_data)
        status, response = public_api_fixture.rest_send_request_with_token('prisma-cloud-alert-rules/v1/alert-rule/match',
                                                                           'post', testbed['auth_key'],
                                                                           data=json.dumps(alert_rule_match_data))
        print("Alert rule match status is {}".format(status))
        out_flag = check_alert_rule_match(status, response, out_flag, alert_data['name'])

        status, response = public_api_fixture.rest_send_request('alert/rule/{}'.format(alert_rule_id),
                                                                'delete', timeout=120)
        print("Delete status is {}".format(status))

        assert out_flag

    def test_1_acc_null_pol_one_reg_one_tags(self, request, testbed, public_api_fixture):
        """
        Create payload with one accountIDs,
        null PolicyIDs, one tags,
        one regions and verify the response

        :param request: request fixture of pytest
        :param testbed: testbed fixture from main conftest
        :param public_api_fixture: fixture returning
        publicapi session used for sending request

        :return:
        """
        out_flag = False
        testname = request.node.name

        alert_rule_match_data = payloads['alert_rule_match_data'][testname]

        alert_rule_match_data[0]['customerId'] = str(testbed['customerId'])
        alert_rule_match_data[0]['accountId'] = random.choice(request.config.cloud_accounts)

        print(alert_rule_match_data)
        status, response = public_api_fixture.rest_send_request_with_token('prisma-cloud-alert-rules/v1/alert-rule/match',
                                                                           'post', testbed['auth_key'],
                                                                           data=json.dumps(alert_rule_match_data))
        if status == 400:
            print("Alert rule match status is {}".format(status))
            logger.info("Policy Id should not be null")
            out_flag = True

        assert out_flag

    def test_null_acc_one_pol_one_reg_one_tags(self, request, testbed, public_api_fixture):
        """
        Create payload with null accountIDs,
        one PolicyIDs, one tags,
        one regions and verify the response

        :param request: request fixture of pytest
        :param testbed: testbed fixture from main conftest
        :param public_api_fixture: fixture returning
        publicapi session used for sending request

        :return:
        """
        out_flag = False
        pol_id = random.choice(request.config.get_policies)
        testname = request.node.name

        alert_rule_match_data = payloads['alert_rule_match_data'][testname]

       # alert_rule_match_data[0]['accountId'] = random.choice(request.config.cloud_accounts)
        alert_rule_match_data[0]['policyId'] = pol_id

        print(alert_rule_match_data)
        status, response = public_api_fixture.rest_send_request_with_token('prisma-cloud-alert-rules/v1/alert-rule/match',
                                                                           'post', testbed['auth_key'],
                                                                           data=json.dumps(alert_rule_match_data))
        if status == 400:
            print("Alert rule match status is {}".format(status))
            logger.info("Account Id should not be null")
            out_flag = True

        assert out_flag

    def test_one_acc_no_pol_no_reg_one_tags(self, request, testbed, public_api_fixture):
        """
        Create payload with null accountIDs,
        no PolicyIDs, one tags,
        no regions and verify the response

        :param request: request fixture of pytest
        :param testbed: testbed fixture from main conftest
        :param public_api_fixture: fixture returning
        publicapi session used for sending request

        :return:
        """
        out_flag = False
        testname = request.node.name

        alert_rule_match_data = payloads['alert_rule_match_data'][testname]

        alert_rule_match_data[0]['accountId'] = random.choice(request.config.cloud_accounts)

        print(alert_rule_match_data)
        status, response = public_api_fixture.rest_send_request_with_token('prisma-cloud-alert-rules/v1/alert-rule/match',
                                                                           'post', testbed['auth_key'],
                                                                           data=json.dumps(alert_rule_match_data))
        if status == 400:
            print("Alert rule match status is {}".format(status))
            logger.info("Policy and region Id should not be null")
            out_flag = True

        assert out_flag

    def test_multiple_alert_rule_one_acc_all_pol_all_reg_no_tags(self, request, testbed, public_api_fixture):
        """
        Create two alert rules.

        1) Create payload with one accountIDs,
        all PolicyIDs, one tags,
        one regions and verify the response

        2) Create payload with one accountIDs,
        one PolicyIDs, no tags, all regions
        and verify the response

        :param request: request fixture of pytest
        :param testbed: testbed fixture from main conftest
        :param public_api_fixture: fixture returning
        publicapi session used for sending request

        :return:
        """

        pol_id = random.choice(request.config.get_policies)
        print(pol_id)
        testname = request.node.name

        alert_data_1 = payloads['alert_rule_create_data'][testname][0]
        alert_data_1['target']['accountGroups'].append(request.config.account_group_id)
        alert_data_2 = payloads['alert_rule_create_data'][testname][1]
        alert_data_2['target']['accountGroups'].append(request.config.account_group_id)
        alert_data_2['policies'].append(pol_id)

        alert_rule_match_data = payloads['alert_rule_match_data'][testname]
        alert_rule_match_data[0]['accountId'] = random.choice(request.config.cloud_accounts)
        alert_rule_match_data[0]['customerId'] = testbed['customerId']
        alert_rule_match_data[1]['accountId'] = random.choice(request.config.cloud_accounts)
        alert_rule_match_data[1]['policyId'] = pol_id
        alert_rule_match_data[1]['customerId'] = testbed['customerId']

        alert_rule_Ids = list()
        status, response = public_api_fixture.rest_send_request('alert/rule', 'post', data=json.dumps(alert_data_1),
                                                                timeout=120)
        print("Alert rule for data1 status is {}".format(status))
        if status == 200:
            alert_rule_id_1 = json.loads(response)['policyScanConfigId']
            alert_rule_Ids.append(alert_rule_id_1)

        status, response = public_api_fixture.rest_send_request('alert/rule', 'post', data=json.dumps(alert_data_2),
                                                                timeout=120)
        print("Alert rule for data2 status is {}".format(status))
        if status == 200:
            alert_rule_id_2 = json.loads(response)['policyScanConfigId']
            alert_rule_Ids.append(alert_rule_id_2)
        print(alert_rule_Ids)

        status, response = public_api_fixture.rest_send_request_with_token('prisma-cloud-alert-rules/v1/alert-rule/match',
                                                                           'post', testbed['auth_key'],
                                                                           data=json.dumps(alert_rule_match_data))
        if status == 200:
            response = json.loads(response)
            count = 0
            for i in response:
                for j in i['alertRuleDetails']:
                    if j['name'] == "alerts_rule_match_auto_3" or j['name'] == "alerts_rule_match_auto_4":
                        count += 1

        for j in range(len(alert_rule_Ids)):
            status, response = public_api_fixture.rest_send_request('alert/rule/{}'.format(alert_rule_Ids[j]),
                                                                    'delete', timeout=120)
            print("Delete status is {}".format(status))

        assert count >= 2



