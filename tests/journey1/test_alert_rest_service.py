import json
import time
import pytest

from libs.utils.kafka_client import KafkaClient
from libs.utils.http_client import HttpClient
from libs.utils.pytest_utils import get_test_data_file_content
from libs.utils.cassandra_util import DBOperator

payloads = get_test_data_file_content('data/dlp_output.json')


def get_test_names(test_data):
    """
     Method to return the list of tests reading the test data file content
    """
    test_names = []
    print("Entering get_test_names...")
    for test_name in list(payloads.keys()):
        print("testname:", test_name)
        print("input:", test_data)
        if test_name == test_data:
            for tests in list(payloads[test_name].keys()):
                print("tests:", tests)
                test_names.append(pytest.param(tests, id=tests))
                print("test names:", test_names)

    return test_names

def getElasticSearchRecords(test_name, testbed):

    elastic_search = "elastic:elastic@%s/local-alerts/_search" % testbed['elasticsearch']

    client = HttpClient()
    # Connect to the Elastic Search and verify the records in the DB
    print("__________________________________________________")
    print("Verifying the records in Elastic Search DB ...")

    # data = json.dumps(search_payload))
    status, response = client.rest_send_request(uri=elastic_search, method='GET')

    print("Response Code:", status)
    print("Response:", response)
    response = json.loads(response.decode("utf-8"))
    for each_es_record in response['hits']['hits']:
        print("customerId:", each_es_record['_source']['customerId'])
        if each_es_record['_source']['customerId'] =='111':
            assert "Bad json pay load made it to Elastic Search"


@pytest.mark.FUNCTIONAL
@pytest.mark.parametrize('test_name', get_test_names('alert_raw_main'))
def test_alert_queue_service_api(test_name, testbed):
    # Consuming from a Kafka topic

    payload = payloads["alert_raw_main"][test_name]

    for each_payload in payload:
        print("each_payload:", each_payload)
        print("Running Tests....")
        print("Produce to Kafka topic - local-alertVerdict")

        # Produce Alert message to a Kafka topic
        kafka_client = KafkaClient(kafka_servers=testbed['kafka_servers'])
        kafka_client.write_topic(testbed['verdict_topic'], each_payload)
        time.sleep(5)

        # Connect to the Cassandra and verify the records in the DB
        print("__________________________________________________")
        print("Verifying the records in Cassandra DB ...")
        cassandra_obj = DBOperator(contact_points=testbed['cassandra'])
        session = cassandra_obj.connect_to_db()
        customer_id = each_payload['customerId']
        print("customerId:", customer_id)
        rows = session.execute(
            "select * from local_redlock.alert where customer_id='%s' ALLOW FILTERING;" %
            customer_id)

        for row in rows:
            print(row)
            assert len(row) != 0
        rows = session.execute(
            "select status from local_redlock.alert where customer_id='%s' ALLOW FILTERING;" %customer_id)
        for row in rows:
            print("Status:", row.status)
            assert len(row) != 0


@pytest.mark.FUNCTIONAL
@pytest.mark.parametrize('test_name', get_test_names('alert_raw_main'))
def test_enqueuer_api(test_name, testbed):
    # Consuming from a Kafka topic

    payload = payloads["alert_raw_main"][test_name]
    alerts_endpoint = "%s:9081/alerts/v1/policyVerdict" % testbed['alerts_server']
    for each_payload in payload:
        print("Running Tests....")
        print("HTTP POST to the Enqueuer service")

        # HTTP POST to the Alert Enqueuer Service
        test_payload = [each_payload]
        # print(test_payload)
        client = HttpClient()
        status, response = client.rest_send_request(
            uri=alerts_endpoint,
            method='post',
            data=json.dumps(test_payload))
        print("Response:", status)
        if "bad_payload" in test_name:
            getElasticSearchRecords(test_name,testbed)
            assert status == 400
        else:
            assert status == 202
        time.sleep(6)

        '''
        # Consuming from a Kafka topic
        print("__________________________________________________")
        print("Verifying Kafka Reads - alertVerdict")
        kafka_consumer = KafkaClient(kafka_servers=testbed['kafka_servers'])
        reads = kafka_consumer.read_topic(testbed['verdict_topic'])
        assert len(reads) != 0

        # Consuming from a Kafka topic
        print("__________________________________________________")
        print("Verifying Kafka Reads - alertNotification")
        reads = kafka_consumer.read_topic(testbed['notification_topic'])
        assert len(reads) != 0

        '''
        # Connect to the Cassandra and verify the records in the DB
        print("__________________________________________________")
        print("Verifying the records in Cassandra DB ...")
        cassandra_obj = DBOperator()
        session = cassandra_obj.connect_to_db()
        customer_id = each_payload['customerId']
        print("customerId:", customer_id)
        rows = session.execute(
            "select * from local_redlock.alert where customer_id='%s' ALLOW FILTERING;" %
            customer_id)
        for row in rows:
            print(row)
            assert len(row) != 0
        rows = session.execute(
            "select status from local_redlock.alert where customer_id='%s' ALLOW FILTERING;" %
            customer_id)
        for row in rows:
            print(row.status)
            assert len(row) != 0

