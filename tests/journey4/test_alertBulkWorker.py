import os
import json
import time
import pytest
import requests

from libs.utils.http_client import HttpClient
from libs.utils.logger import get_logger
from libs.utils.pytest_utils import get_test_data_file_content
from libs.utils.elasticsearch_util import ElasticSearchDB
from libs.utils.kafka_client import KafkaClient
from libs.utils.cassandra_util import DBOperator

payloads = get_test_data_file_content('data/alert_tests.json')
logger = get_logger(os.path.basename(__file__).replace(".py", ""))


def get_test_names(test_data):
    """
     Method to return the list of tests reading the test data file content
    """
    test_names = []
    print("Entering get_test_names...")
    for test_name in list(payloads.keys()):
        print("testname:", test_name)
        logger.info("testname:", test_name)
        print("input:", test_data)
        if test_name == test_data:
            for tests in list(payloads[test_name].keys()):
                print("tests:", tests)
                logger.info("tests:", tests)
                test_names.append(pytest.param(tests, id=tests))
                print("test names:", test_names)
                logger.info("test names:", test_names)

    return test_names


@pytest.mark.INTEGRATION
@pytest.mark.parametrize('test_name', get_test_names('alert_bulkUpdate_main'))
def test_bulk_worker_bulk_alert_update(test_name, testbed):

    print("Entering the bulk worker...")
    logger.info("Entering the bulk worker...")
    # Bulk post the data to the REST endpoint
    payload = payloads["alert_raw_main"]["alert_bulk"]
    alerts_endpoint = "%s:9081/alerts/v1/policyVerdict" % testbed['alerts_server']
    # for each_payload in payload:

    print("Running Tests....post_alerts")
    logger.info("Running Tests....post_alerts")
    print("HTTP POST to the Enqueuer service")
    logger.info("HTTP POST to the Enqueuer service")

    # HTTP PUT to the Alert Enqueuer Service
    # test_payload = [each_payload]
    test_payload = payload
    print(test_payload)
    logger.info(test_payload)
    client = HttpClient()
    status, response = client.rest_send_request(uri=alerts_endpoint, method='post',
                                                data=json.dumps(test_payload))
    logger.info(response)
    print("Response:", status)
    logger.info("Response:", status)
    assert status == 202


    # Verification of the bulk worker service

    payload = payloads["alert_bulkUpdate_main"][test_name]
    alerts_search = "%s:9081/alerts/v1/bulk/statusJob" % testbed['alerts_server']
    print("Running Tests....Bulk worker Service")
    logger.info("Running Tests....Bulk worker Service")

    # HTTP PUT to the Alert bulk worker Service
    # print(test_payload)
    client = HttpClient()

    for each_payload in payload:
        print("each_payload:", each_payload)
        logger.info("each_payload:", each_payload)
        status, response = client.rest_send_request(uri=alerts_search, method='PUT',
                                                    data=json.dumps(each_payload))
        logger.info(response)
        assert status == 201
        print("Response Status:", status)
        logger.info("Response Status:", status)
        responseObj = json.loads(response.decode("utf-8"))
        print("response payload:", responseObj)
        logger.info("response payload:", responseObj)
        assert responseObj == True
       # for each_resp_obj in responseObj:
       #    assert each_resp_obj == True

        time.sleep(10)
        # Connect to the Cassandra and verify the records in the DB
        print("__________________________________________________")
        print("Verifying the records in Cassandra DB ...")
        logger.info("__________________________________________________")
        logger.info("Verifying the records in Cassandra DB ...")
        cassandra_obj = DBOperator()
        session = cassandra_obj.connect_to_db()
        customer_id = each_payload['searchRequest']['customerId']
        print("customerId:", customer_id)
        logger.info("customerId:", customer_id)
        rows = session.execute("select * from local_redlock.alert where customer_id='%s' ALLOW FILTERING;" %customer_id)
        for row in rows:
            print(row)
            assert len(row) != 0
        rows = session.execute("select status from local_redlock.alert where customer_id='%s' ALLOW FILTERING;" %customer_id)
        for row in rows:
            print(row.status)
            assert row.status == each_payload["status"]


@pytest.mark.INTEGRATION
@pytest.mark.parametrize('test_name', get_test_names('alert_singleUpdate_main'))
def test_single_alert_update(test_name, testbed):

    # POST the data to the REST endpoint
    print("Entering the single alert update ...")
    logger.info("Entering the single alert update ...")
    # Bulk post the data to the REST endpoint
    payload = payloads["alert_raw_main"]["alert_bulk"]
    alerts_endpoint = "%s:9081/alerts/v1/policyVerdict" % testbed['alerts_server']
    # for each_payload in payload:

    print("Running Tests....post_alerts")
    print("HTTP POST to the Enqueuer service")

    logger.info("Running Tests....post_alerts")
    logger.info("HTTP POST to the Enqueuer service")
    # HTTP PUT to the Alert Enqueuer Service
    # test_payload = [each_payload]
    test_payload = payload
    print(test_payload)
    client = HttpClient()
    status, response = client.rest_send_request(uri=alerts_endpoint, method='post',
                                                data=json.dumps(test_payload))
    logger.info(response)
    print("Response:", status)
    logger.info("Response:", status)
    assert status == 202

    
    # Verification of the single alert update

    # The alert Id is procured from the search service

    alerts_search = "%s:9084/alerts/v1/search" % testbed['alerts_server']
    # HTTP POST to the Alert Search/Aggregation Service
    # print(test_payload)
    client = HttpClient()
    payload_search = payloads["alert_search_main"]["Search_to_Dismiss"]
    for each_payload_search in payload_search:

        print("each_payload_search:",each_payload_search)
        logger.info("each_payload_search:",each_payload_search)
        status, response = client.rest_send_request(uri=alerts_search, method='POST',
                                                    data=json.dumps(each_payload_search))
        logger.info(response)
        assert status == requests.codes.ok

        print("Response Status:", status)
        logger.info("Response Status:", status)
        responseObj = json.loads(response.decode("utf-8"))
        print("response payload:", responseObj)
        logger.info("response payload:", responseObj)
        alertId = responseObj[0]['alertId']
        print("alertId:", responseObj[0]['alertId'])
        logger.info("alertId:", responseObj[0]['alertId'])
        logger.info("response payload:", responseObj)
        assert len(responseObj) != 0
        for each_resp_obj in responseObj:
            print(each_resp_obj)
            logger.info(each_resp_obj)
            assert len(each_resp_obj) != 0


    alert_payload = payloads["alert_singleUpdate_main"][test_name]
    #alertId = alert_payload[0]["searchRequest"]["filters"]["alertIds"]
    #alertId = alertId[0]
    alerts_search = "%s:9081/alerts/v1/alertRecord/%s/status" % (testbed['alerts_server'],alertId)

    print("Running Tests....single update Service")
    logger.info("Running Tests....single update Service")
    # HTTP PUT to the Alert bulk worker Service
    # print(test_payload)
    client = HttpClient()

    for each_payload in alert_payload:
        print("each_payload:", each_payload)
        logger.info("each_payload:", each_payload)
        status, response = client.rest_send_request(uri=alerts_search, method='PUT',
                                                    data=json.dumps(each_payload))
        logger.info(response)
        assert status == requests.codes.ok
        print("Response Status:", status)
        logger.info("Response Status:", status)
        responseObj = json.loads(response.decode("utf-8"))
        print("response payload:", responseObj)
        logger.info("response payload:", responseObj)
        assert responseObj == True

        time.sleep(10)
        # Connect to the Cassandra and verify the records in the DB
        print("__________________________________________________")
        print("Verifying the records in Cassandra DB ...")
        logger.info("__________________________________________________")
        logger.info("Verifying the records in Cassandra DB ...")
        cassandra_obj = DBOperator()
        session = cassandra_obj.connect_to_db()
        customer_id = each_payload['searchRequest']['customerId']

        rows = session.execute("select * from local_redlock.alert where customer_id='%s' ALLOW FILTERING;" %customer_id)
        for row in rows:
            print(row)
            assert len(row) != 0
        rows = session.execute("select status from local_redlock.alert where customer_id='%s' ALLOW FILTERING;" %customer_id)
        for row in rows:
            print(row.status)
            assert row.status == each_payload["status"]
