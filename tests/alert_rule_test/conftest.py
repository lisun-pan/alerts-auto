import json
from time import sleep

import pytest

from libs.utils.public_api import PublicApi
from libs.utils.pytest_utils import get_test_data_file_content

payloads = get_test_data_file_content('data/alert_rule.json')


def get_test_names(test_data):
    """
     Method to return the list of tests reading the test data file content
    """
    test_names = []
    print("Entering get_test_names...")
    for test_name in list(payloads.keys()):
        print("testname:", test_name)
        print("input:", test_data)
        if test_name == test_data:
            for tests in list(payloads[test_name].keys()):
                print("tests:", tests)
                test_names.append(pytest.param(tests, id=tests))
                print("test names:", test_names)

    return test_names


def policy_create_public(testbed):
    """
     Method to create a new policy
    """
    client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                       password=testbed["password"])
    policy_create_endpoint = "/policy"

    policy_create_payloads = payloads['alert_rule_main']['policy_create']

    client.rest_login()
    status, response = client.rest_send_request(uri=policy_create_endpoint, method='POST',
                                                data=json.dumps(policy_create_payloads))
    assert status == 200, policy_create_payloads
    return policy_get_by_name_public(testbed, payloads['alert_rule_main']['policy_create']['name'])


def policy_get_by_name_public(testbed, policy_name):
    """
     Method to get the policy by name
    """
    client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                       password=testbed["password"])
    # Get policy by name
    policy_getall_endpoint = "policy?policy.name=%s" % policy_name
    client.rest_login()
    status, response = client.rest_send_request(uri=policy_getall_endpoint, method='GET')
    assert status == 200, response
    responseObj = json.loads(response)
    assert len(responseObj) == 1, "Multiple policies have the same name"
    return responseObj[0]


def policy_delete_public(testbed, policy_id):
    """
     Method to delete a policy by policy_id
    """
    client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                       password=testbed["password"])

    policy_delete_endpoint = "policy/%s" % policy_id
    client.rest_login()
    status, response = client.rest_send_request(uri=policy_delete_endpoint, method='DELETE')
    assert status == 204, "Can't delete policy"


def alert_rule_create_public(testbed, policy):
    """
     Method to create an alert rule
    """
    client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                       password=testbed["password"])

    alert_rule_create_payloads = payloads['alert_rule_main']['alert_rule_create']

    alert_rule_create_payloads['policies'].append(policy)
    ag_objs = account_groups_get_all_public(testbed)

    alert_rule_create_payloads['target']['accountGroups'].append(ag_objs[0]['id'])

    client.rest_login()
    alert_rule_create_endpoint = "alert/rule"
    status, response = client.rest_send_request(uri=alert_rule_create_endpoint, method='POST',
                                                data=json.dumps(alert_rule_create_payloads))
    assert status == 200, "Can't create alert rule"

    return alert_rule_get_by_name_public(testbed, payloads['alert_rule_main']['alert_rule_create']["name"])


def alert_rule_delete_public(testbed, alert_rule_id):
    """
     Method to delete an alert rule by alert_rule_id
    """
    client = PublicApi(hostname="api14.qa.prismacloud.io", user=testbed["user"],
                       password=testbed["password"])
    print("alert_rule_id")
    print(alert_rule_id)
    alert_rule_delete_endpoint = "alert/rule/%s" % alert_rule_id

    client.rest_login()
    status, response = client.rest_send_request(uri=alert_rule_delete_endpoint, method='DELETE')

    assert status == 204, "Can't delete alert rule"


def alert_rule_get_by_name_public(testbed, alert_rule_name):
    """
     Method to get alert rule by alert_rule_id
    """
    client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                       password=testbed["password"])
    # Get policy by name
    alert_rule_getall_endpoint = "alert/rule"
    client.rest_login()
    status, response = client.rest_send_request(uri=alert_rule_getall_endpoint, method='GET')
    assert status == 200, response
    responseObj = json.loads(response)
    for i in range(len(responseObj)):
        if responseObj[i]['name'] == alert_rule_name:
            return responseObj[i]


def account_get_all_public(testbed):
    """
     Method to get all the cloud account
    """
    client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                       password=testbed["password"])
    account_getall_endpoint = "cloud"
    client.rest_login()
    status, response = client.rest_send_request(uri=account_getall_endpoint, method='GET')
    assert status == 200, response
    responseObj = json.loads(response)
    assert len(responseObj) > 1, "At less need 2 account in the env"
    return responseObj


def account_groups_get_all_public(testbed):
    """
     Method to get all the cloud account group
    """
    client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                       password=testbed["password"])
    account_getall_endpoint = "cloud/group"
    client.rest_login()
    status, response = client.rest_send_request(uri=account_getall_endpoint, method='GET')
    assert status == 200, response
    responseObj = json.loads(response)
    assert len(responseObj) > 1, "At less need 2 account groups in the env"
    return responseObj


def account_groups_get_by_id(testbed, ag_id):
    """
     Method to get the cloud account group by id
    """
    client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                       password=testbed["password"])

    account_get_endpoint = "cloud/group/%s" % ag_id
    status, response = client.rest_send_request(uri=account_get_endpoint, method='GET')
    assert status == 200, response
    responseObj = json.loads(response)
    return responseObj


@pytest.fixture(scope='class')
def alert_rule_prepare(testbed):
    """
    Create the related policy and alert rule, then delete all of them at the end of test case
    :param testbed:
    :return:
    """
    policyObj = policy_create_public(testbed)
    print("******************************Create policy")
    alertObj = alert_rule_create_public(testbed, policyObj['policyId'])
    print("******************************Create alert_rule")

    sleep(10)

    yield
    alert_rule_delete_public(testbed, alertObj['policyScanConfigId'])
    print("******************************Delete alert_rule")
    policy_delete_public(testbed, policyObj['policyId'])
    print("******************************Delete policy")


if __name__ == '__main__':
    testbed = {'alerts_server': 'api14.qa.prismacloud.io', "user": "",
               "password": ""}
    res = account_get_all_public(testbed)
    print(res)
