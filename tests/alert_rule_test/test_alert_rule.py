import json
import os

import pytest

from libs.utils.kafka_client import KafkaClient
from libs.utils.logger import get_logger
from libs.utils.public_api import PublicApi
from libs.utils.pytest_utils import get_test_data_file_content
from tests.alert_rule_test.conftest import policy_get_by_name_public, alert_rule_get_by_name_public, \
    account_groups_get_all_public, account_get_all_public, account_groups_get_by_id, policy_delete_public

payloads = get_test_data_file_content('data/alert_rule.json')
logger = get_logger(os.path.basename(__file__).replace(".py", ""))


@pytest.mark.usefixtures("alert_rule_prepare")
class TestAlertsRule(object):
    """
    test alert_rule_sanity_check:
        1. test alert rule creation
        2. test alert rule description update
        3. test alert rule deleted
    """
    @pytest.mark.API
    def test_alert_rule_sanity_check(self, testbed):
        client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                           password=testbed["password"])

        alert_rule_update_payloads = payloads["alert_rule_main"]['alert_rule_update']
        policyObj = policy_get_by_name_public(testbed, payloads['alert_rule_main']['policy_create']['name'])
        alert_rule_update_payloads['policies'].append(policyObj['policyId'])

        alert_rule_Obj = alert_rule_get_by_name_public(testbed,
                                                       payloads['alert_rule_main']['alert_rule_create']["name"])
        alert_rule_update_endpoint = "alert/rule/%s" % alert_rule_Obj['policyScanConfigId']

        ag_objs = account_groups_get_all_public(testbed)
        alert_rule_update_payloads['target']['accountGroups'].append(ag_objs[0]['id'])

        client.rest_login()
        status, response = client.rest_send_request(uri=alert_rule_update_endpoint, method='PUT',
                                                    data=json.dumps(alert_rule_update_payloads))
        assert status == 200, "Can't update the alert_rule"

    """
    test test_alert_rule_enable_disable
        steps:
        1. disable the alert rule
        2. enable the alert rule 
        3. for all the operations, verify from the kafka side
    """

    def test_alert_rule_enable_disable(self, testbed):
        client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                           password=testbed["password"])
        alert_rule_Obj = alert_rule_get_by_name_public(testbed,
                                                       payloads['alert_rule_main']['alert_rule_create']["name"])
        alert_rule_disable_endpoint = "alert/rule/%s/status/false" % alert_rule_Obj['policyScanConfigId']

        client.rest_login()
        status, response = client.rest_send_request(uri=alert_rule_disable_endpoint, method='PATCH')
        assert status == 200, "Can't disable alert rule"

        alert_rule_enable_endpoint = "alert/rule/%s/status/false" % alert_rule_Obj['policyScanConfigId']
        status, response = client.rest_send_request(uri=alert_rule_enable_endpoint, method='PATCH')
        assert status == 200, "Can't enable alert rule"

        krp = KafkaClient()
        krp_diff = krp.read_kafka_rest_proxy('alertrule_change_topic', testbed)
        assert len(krp_diff) > 0, "previous and current jsons are the same"

    """
    test alert_rule policy change
        steps:
        1. create a new policy;
        2. update the current alert rule to associate with this policy;
        3. delete this policy
    """

    def test_alert_rule_policy_change(self, testbed):
        client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                           password=testbed["password"])
        client.rest_login()
        policy_create_endpoint = "/policy"
        policy_create_payloads = payloads['alert_rule_main']['policy_create_add']
        status, response = client.rest_send_request(uri=policy_create_endpoint, method='POST',
                                                    data=json.dumps(policy_create_payloads))
        assert status == 200, policy_create_payloads
        policy_add_Obj = json.loads(response)

        alert_rule_update_payloads = payloads["alert_rule_main"]['alert_rule_update']

        policy_Obj = policy_get_by_name_public(testbed, payloads['alert_rule_main']['policy_create']['name'])
        alert_rule_update_payloads['policies'].append(policy_Obj['policyId'])
        alert_rule_update_payloads['policies'].append(policy_add_Obj['policyId'])

        alertObj = alert_rule_get_by_name_public(testbed, payloads['alert_rule_main']['alert_rule_create']["name"])
        alert_rule_update_endpoint = "alert/rule/%s" % alertObj['policyScanConfigId']

        status, response = client.rest_send_request(uri=alert_rule_update_endpoint, method='PUT',
                                                    data=json.dumps(alert_rule_update_payloads))
        assert status == 200, "Can't update the alert_rule"

        krp = KafkaClient()
        krp_diff = krp.read_kafka_rest_proxy('alertrule_change_topic', testbed)
        assert len(krp_diff) > 0, "previous and current jsons are the same"

        policy_delete_public(testbed, policy_add_Obj['policyId'])

        """
            test test_alert_rule_account_change
            steps:
            1. add more account groups with current alert rule
        """

    def test_alert_rule_account_change(self, testbed):
        client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                           password=testbed["password"])

        alert_rule_update_payloads = payloads["alert_rule_main"]['alert_rule_add_account_update']
        policyObj = policy_get_by_name_public(testbed, payloads['alert_rule_main']['policy_create']['name'])
        alert_rule_update_payloads['policies'].append(policyObj['policyId'])

        alertObj = alert_rule_get_by_name_public(testbed, payloads['alert_rule_main']['alert_rule_create']["name"])
        alert_rule_update_endpoint = "alert/rule/%s" % alertObj['policyScanConfigId']

        ag_objs = account_groups_get_all_public(testbed)
        alert_rule_update_payloads['target']['accountGroups'].append(ag_objs[0]['id'])
        alert_rule_update_payloads['target']['accountGroups'].append(ag_objs[1]['id'])

        client.rest_login()
        status, response = client.rest_send_request(uri=alert_rule_update_endpoint, method='PUT',
                                                    data=json.dumps(alert_rule_update_payloads))
        assert status == 200, "Can't update the alert_rule"

        krp = KafkaClient()
        krp_diff = krp.read_kafka_rest_proxy('alertrule_change_topic', testbed)
        assert len(krp_diff) > 0, "previous and current jsons are the same"

        """
            test alert_rule_account_group_add_new_account_change
            steps:
            1. get a account group which associate with current alert rule;
            2. update the account group with different account;
            3. judge the alert rule from kafka
        """
    def test_alert_rule_account_group_add_new_account_change(self, testbed):
        client = PublicApi(hostname=testbed['alerts_server'], user=testbed["user"],
                           password=testbed["password"])
        client.rest_login()
        account_list = account_get_all_public(testbed)
        alert_rule_Obj = alert_rule_get_by_name_public(testbed,
                                                       payloads['alert_rule_main']['alert_rule_create']["name"])

        ag_id = alert_rule_Obj['target']['accountGroups'][0]
        accountGroup_Obj = account_groups_get_by_id(testbed, ag_id)
        accountGroup_update_payload = payloads["alert_rule_main"]['accountgroup_update']
        accountGroup_update_payload['id'] = ag_id
        accountGroup_update_payload['name'] = accountGroup_Obj['name']

        accountGroup_update_payload['accountIds'].append(account_list[0]['accountId'])
        accountGroup_update_payload['accountIds'].append(account_list[1]['accountId'])
        accountGroup_update_endpoint = "https://%s/cloud/group/%s" % (testbed['alerts_server'], ag_id)

        status, response = client.rest_send_request(uri=accountGroup_update_endpoint, method='PUT',
                                                    data=json.dumps(accountGroup_update_payload))
        assert status == 200, "Can't update the account_group"

        krp = KafkaClient()
        krp_diff = krp.read_kafka_rest_proxy('alertrule_change_topic', testbed)
        assert len(krp_diff) > 0, "previous and current jsons are the same"
