import urllib.parse as urlparse
import os
import requests

from libs.utils.logger import get_logger

logger = get_logger(os.path.basename(__file__).replace(".py", ""))


class HttpClient:

    def __init__(self):
        self.logger = logger
        self.base_url = "http://"
        self.session = requests.Session()
        #self.session.headers.update({'Content-Type': 'application/json'})
        self.session.verify = False

    def rest_send_request(
            self,
            uri,
            method,
            data=None,
            headers=None,
            params=None,
            timeout=120):
        """
        Function to send any type of request to a remote machine
        :param uri:
        :param method:
        :param data:
        :param headers:
        :param params:
        :param timeout:
        :param reason: Return Reason (Rest Return Code as text)
        :return:
        """

        if ('redlock.io' in uri) or ('myredlock.com' in uri) or ('prismacloud' in uri):
            self.base_url = "https://" + uri
        else:
            self.base_url = 'http://' + uri

        url = self.base_url
        self.logger.debug("Making {} call to {}".format(method, url))
        if data:
            self.logger.info("Data: {}".format(data))
        if params:
            self.logger.debug("Params: {}".format(params))
        response = self.session.__getattribute__(method.lower())(
            url,
            data=data,
            headers=headers,
            params=params,
            timeout=timeout
        )
        self.logger.info(
            "Response: {} ({})".format(
                response.status_code, str(
                    response.reason).upper()))
        return response.status_code, response.content


if __name__ == "__main__":
    pass
