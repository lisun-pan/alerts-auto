import os
import json
import kafka
import re
import base64
from kafka.errors import KafkaError
from libs.utils.logger import get_logger
from libs.utils.http_client import HttpClient
from time import sleep
from jsondiff import diff

KAFKA_LOGGER = get_logger(os.path.basename(__file__).replace(".py", ""))


class KafkaClient:

    def __init__(self, kafka_servers=None):
        self.logger = KAFKA_LOGGER
        self.kafka_servers = kafka_servers

    def read_topic(self, topic, nbr_msg=1):
        kafka_consumer = kafka.KafkaConsumer(
            topic,
            bootstrap_servers=self.kafka_servers,
            auto_offset_reset='earliest',
            enable_auto_commit=True,
            auto_commit_interval_ms=1000,
            security_protocol="SSL",
            ssl_cafile='certs/Redlock_CA.pem',
            group_id='alerts-Integration_test')
        messages = []
        try:
            counter = 0
            for message in kafka_consumer:
                messages.append(message)
                counter = counter + 1
                if counter == nbr_msg:
                    break
        except KafkaError as error:
            self.logger.error(error)
        finally:
            kafka_consumer.close(True)
        return messages

    def write_topic(self, topic, payload):
        kafka_producer = kafka.KafkaProducer(
            bootstrap_servers=self.kafka_servers,
            security_protocol="SSL",
            ssl_cafile='certs/Redlock_CA.pem',
            value_serializer=lambda v: json.dumps(v).encode('utf-8'))
        try:
            message = kafka_producer.send(topic, key=b'1234', value=payload)
            sent_result = message.get(timeout=60)
            return sent_result
        except KafkaError as error:
            self.logger.error(error)

    def read_kafka_rest_proxy(self, topic, testbed):
        kafka_rest_proxy_endpoint = "%s/kafka-rest-proxy/consumers/rlp_record_changev5" % testbed['alerts_server']

        # Kafka Create Consumer group
        client = HttpClient()
        kafka_headers = {
            'Content-Type': 'application/vnd.kafka.v2+json',
            'x-kafka-rest-host': testbed['kafka_servers'][0],
            'Authorization': testbed['krp_auth_key']
        }
        kafka_consumer_group_payload = {
            "format": "binary",
            "auto.offset.reset": "earliest"
        }
        status, response = client.rest_send_request(uri=kafka_rest_proxy_endpoint, method='POST', headers=kafka_headers,
                                                    data=json.dumps(kafka_consumer_group_payload))

        response = json.loads(response.decode("utf-8"))
        print("response:consumer-group:", response)
        kafka_consumer_group_instance = response['instance_id'].strip()
        print("instanceId:", kafka_consumer_group_instance)
        kafka_rest_uri = response['base_uri']
        kafka_rest_host = re.search(r'https://(.*):[0-9]+', kafka_rest_uri)
        print('kafka_rest_host:', kafka_rest_host.group(1))
        kafka_rest_host = kafka_rest_host.group(1).strip()
        print("Status:", status)
        assert status == 200
        print(
            "------------------------------------------------------------------------------------------------------------------------")

        # Create Subscription
        kafka_create_subscription_url = "%s/kafka-rest-proxy/consumers/rlp_record_changev5/instances/%s/subscription" % (
            testbed['alerts_server'], kafka_consumer_group_instance)
        print("kafka_create_subscription_url:", kafka_create_subscription_url)

        kafka_create_subscription_payload = {
            "topics": [
                testbed[topic]
            ]
        }
        print("payload:", kafka_create_subscription_payload)
        kafka_create_subscription_headers = {
            'Content-Type': 'application/vnd.kafka.v2+json',
            'x-kafka-rest-host': kafka_rest_host,
            'Authorization': testbed['krp_auth_key']
        }
        print("subscription_headers:", kafka_create_subscription_headers)

        status, response = client.rest_send_request(uri=kafka_create_subscription_url, method='POST',
                                                    headers=kafka_create_subscription_headers,
                                                    data=json.dumps(kafka_create_subscription_payload))

        print("response:subscription:", response)
        print("Status:", status)
        assert status == 204
        print(
            "------------------------------------------------------------------------------------------------------------------------")
        sleep(7)

        # get messages
        kafka_get_messages_url = "%s/kafka-rest-proxy/consumers/rlp_record_changev5/instances/%s/records" % (
            testbed['alerts_server'], kafka_consumer_group_instance)
        print("kafka_get_messages_url:", kafka_get_messages_url)
        kafka_get_messages_headers = {
            'Accept': '*/*',
            'Accept': 'application/vnd.kafka.binary.v2+json',
            'x-kafka-rest-host': kafka_rest_host,
            'Authorization': testbed['krp_auth_key']
        }

        # Try to get the messages 2 times as the first call always returns an empty list
        for i in range(1, 3):
            status, response = client.rest_send_request(uri=kafka_get_messages_url, method='GET',
                                                        headers=kafka_get_messages_headers)
            print("response:get_messages:", response)
            print("Status:", status)
            assert status == 200
            sleep(2)
            print(
                "------------------------------------------------------------------------------------------------------------------------")
        response = json.loads(response.decode("utf-8"))
        print("response length:", len(response))
        if len(response) > 0:
            response = response[0]
            print("kafka decoded response:", response)
            json_response_encoded = response['value']
            print("kafka response['value']:", json_response_encoded)
            json_data = base64.b64decode(json_response_encoded + "=").decode("utf-8")
            print("Kafka decode response:", json_data)
            json_data = json.loads(json_data)
            previous = json_data["previous"]
            current = json_data["current"]
            print("previous:", previous)
            print("current:", current)

            pretty_diff = diff(previous, current, syntax='symmetric')
            print("Previous and Current diff:", pretty_diff)
            print(
                "------------------------------------------------------------------------------------------------------------------------")

        # Delete the consumer-group
        status, del_response = client.rest_send_request(uri=kafka_create_subscription_url, method='DELETE',
                                                        headers=kafka_create_subscription_headers,
                                                        data=json.dumps(kafka_create_subscription_payload))

        print("Delete Consumer-group:", response)
        print("Status:", status)
        assert status == 204

        if len(response) > 0:
            return pretty_diff
        else:
            return


if __name__ == "__main__":
    # ka = KafkaClient(kafka_servers='kafka:9092')
    # # mes = ka.read_topic(topic='local-alertVerdict')
    #
    # print(mes)
    # producer = kafka.KafkaProducer(bootstrap_servers='localhost:9092',
    #                                security_protocol="SSL",
    #                                ssl_cafile='../../certs/Redlock_CA.pem', )
    # future = producer.send('alertVerdict', b'some_message_bytes to test')
    # result = future.get(timeout=60)
    # print(result)
    pass
