import logging
import os
import sys

loggers = {}


class AlertsLogger:
    """
    PRISMASTORAGE Logger that will be used across the framework
    """

    def __init__(self, name="AUTOMATION", log_level=None, to_stdout=False):
        """
        Init the Logger
        :param name: Logger Name that you want to print
        :param log_level: Logging Level, default is INFO
        :param to_stdout: Print to stdout
        """

        self.name = str(name).upper()
        self.to_stdout = to_stdout
        self.log_level = log_level
        self.log_levels_map = {
            'DEBUG': logging.DEBUG,  # 10
            'INFO': logging.INFO,  # 20
            'WARNING': logging.WARN,  # 30
            'WARN': logging.WARNING,  # 30
            'ERROR': logging.ERROR,  # 40
            'CRITICAL': logging.CRITICAL  # 50
        }

        if not self.log_level:
            self.log_level = self.log_levels_map[os.getenv(
                'LOG_LEVEL', 'INFO')]
        else:
            self.log_level = self.log_levels_map[self.log_level]

        self.formatter = logging.Formatter(
            "%(asctime)s %(name)12s %(thread)d %(levelname)7s %(filename)22s: %(lineno)4d - %(message)s")

        # We need to store loggers globally, so that we don't make duplicate
        # handlers
        global loggers
        if loggers.get(self.name):
            self.logger = loggers[self.name]
        else:
            self.logger = logging.getLogger(self.name)
            loggers[self.name] = self.logger

        # Clear if there's any handlers already
        if self.logger.hasHandlers():
            self.logger.handlers.clear()

        self.create_handler()

    def delete_all_handlers(self):
        """
        Function to delete all handlers
        :return:
        """
        for handler in self.logger.handlers:
            self.logger.removeHandler(handler)

    def create_handler(self):
        """
        Create Handler for Logging
        :return:
        """

        # Creating Handler
        console_handler = logging.StreamHandler()
        file_handler = logging.FileHandler('pytest.log')

        # Setting Formatter
        console_handler.setFormatter(self.formatter)
        file_handler.setFormatter(self.formatter)

        # Setting Log Level
        console_handler.setLevel(self.log_level)
        file_handler.setLevel(self.log_level)

        # Adding Handler
        self.logger.addHandler(console_handler)
        self.logger.addHandler(file_handler)

        # Handler to print on screen (as well)
        if self.to_stdout:
            stdout_handler = logging.StreamHandler(sys.stdout)

            # Set Formatter
            stdout_handler.setFormatter(self.formatter)

            # Set Log Level
            stdout_handler.setLevel(self.log_level)

            # Add to handler
            self.logger.addHandler(stdout_handler)

    def get_logger(self):
        """
        Function that will return logger object
        :return:
        """
        return self.logger

    def add_file_handler(self, file_name):
        """
        Add File Handler for Logging
        :param file_name:
        :return:
        """
        file_handler = logging.FileHandler(file_name)
        file_handler.setFormatter(self.formatter)
        self.logger.addHandler(file_handler)

    def remove_file_handler(self, file_name):
        """
        Remove File Handler from Logging
        :param file_name:
        :return:
        """
        file_name = os.path.abspath(file_name)
        for handler_local in list(self.logger.handlers):
            if handler_local.__class__.__name__ == "FileHandler":
                if handler_local.baseFilename == file_name:
                    handler_local.flush()
                    handler_local.close()
                    self.logger.removeHandler(handler_local)


def get_logger(name='alerts', log_level=None, to_stdout=False):
    """
    This function will call the PRSIMASTORAGE LOGGER's return_log_object function and return back to the user
    :param name: Name of the logger
    :param log_level:
    :param to_stdout:
    :return:
    """
    return AlertsLogger(
        name=name,
        log_level=log_level,
        to_stdout=to_stdout).get_logger()


if __name__ == "__main__":
    log = get_logger(name='TEST')
    log.info('This is INFO Log')
    log.error('This is ERROR Log')
    log.debug('This is DEBUG Log')
