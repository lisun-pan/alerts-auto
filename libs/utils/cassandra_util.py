import os
import ssl
from cassandra.cluster import Cluster
from libs.utils.logger import get_logger

CASSANDRA_LOGGER = get_logger(os.path.basename(__file__).replace(".py", ""))


class DBOperator:
    def __init__(self, contact_points=None):
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
        ssl_context.load_verify_locations('certs/Redlock_CA.pem')
        self.cluster = Cluster(contact_points=contact_points,
                               ssl_context=ssl_context)
        self.cassandra_logger = CASSANDRA_LOGGER

    def connect_to_db(self):
        try:
            db_session = self.cluster.connect()
            return db_session
        except Exception as error:
            self.cassandra_logger.error(error)


if __name__ == '__main__':
    # ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    # ssl_context.load_verify_locations('../../certs/Redlock_CA.pem')
    # cluster = Cluster(contact_points=['cassandra'],
    #                   ssl_context=ssl_context)
    # session = cluster.connect()
    # rows = session.execute('select count(*) from local_redlock.alert')
    # print(rows[0].count)
    pass
