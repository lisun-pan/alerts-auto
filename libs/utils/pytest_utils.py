import json
import os

import pytest
from libs.utils.logger import get_logger

logger = get_logger(os.path.basename(__file__).replace(".py", ""))


def get_test_data_file_content(data_file_name):
    """
    Function to read Json File which has test data
    :param data_file_name: Json File which has data in it
    :param read_from_cmd_line_args: Try to get file from command-line
    :param test_files_dir: Test Data Files Directory
    :return:
    """
    # Raise an error, if the file doesn't exist
    assert os.path.isfile(
        data_file_name), "ERROR: File '{}' doesn't exist !!".format(data_file_name)

    # Read Test-Data
    logger.info("Loading test-data from file {}".format(data_file_name))
    with open(data_file_name, "r") as fd:
        test_data = json.load(fd)

    if not test_data:
        logger.error(
            "ERROR: There is no test-data in {} File".format(data_file_name))
        raise Exception(
            "ERROR: There is no test-data in {} File".format(data_file_name))

    return test_data


def get_test_data_file_name(
        data_file,
        verify_file_exists=True,
        read_from_cmd_line_args=True):
    """
    Function to read Json File which has test data
    :param data_file: Json File which has data in it
    :param verify_file_exists: Check if file exists True/False
    :param read_from_cmd_line_args: Fetch file name from command line True/False
    :return:
    """

    test_data_file = None

    # Check if user has given command line option to fetch data from a
    # specific file
    if read_from_cmd_line_args:
        test_data_file = pytest.config.getoption("--test_data_file")

    # If no test data file passed pick the default json file
    if not test_data_file:
        test_data_file = os.path.abspath(
            os.path.join(
                os.environ["REDLOCK_TEST_DATA_PATH"],
                data_file))
        if verify_file_exists:
            assert os.path.isfile(
                test_data_file), "ERROR: File '{}' doesn't exist !!"

    logger.info("Test-data file {}".format(test_data_file))
    return test_data_file


def fetch_json_data_from_file(file_path):
    """
    :param file_path: Full path of the file
    :return: json data
    """
    with open(file_path, "r") as json_file:
        test_data = json.load(json_file)
        json_file.close()
        return test_data


def write_json_data_to_file(file_path, data):
    """
    :param file_path: Full path of the file
    :param data: json data
    :return: None
    """
    with open(file_path, "w+") as json_file:
        json_file.write(json.dumps(data))
        json_file.close()


if __name__ == '__main__':
    data = get_test_data_file_content("../../data/dlp_output.json")
    print(data)
