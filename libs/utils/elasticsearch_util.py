from elasticsearch import Elasticsearch


class ElasticSearchDB:
    def __init__(self):
        print("Initialization..")
        self.es = Elasticsearch()

    def connect_to_es(self):
        print("Elastic search...")
        return self.es


if __name__ == '__main__':
    ES = Elasticsearch()
    print("Elastic search...")
    print(ES.info())
