import urllib.parse as urlparse
import json
import os
import requests

from libs.utils.logger import get_logger

logger = get_logger(os.path.basename(__file__).replace(".py", ""))


def auto_refresh(parent_method):
    """
    Decorator to do an auto_refresh of session
    :param parent_method:
    :return:
    """

    def wrapper_function(self, *args, **kwargs):
        session_extended = self.extend()
        if session_extended:  # Session has been extended, call the parent_method
            return parent_method(self, *args, **kwargs)
        raise Exception("Failed to extend session !!")

    return wrapper_function


class PublicApi:
    URI = {
        'login': {
            'uri': 'login',
            'method': 'post'
        },
        'extend': {
            'uri': 'auth_token/extend',
            'method': 'get'
        },
        'tos': {
            'uri': 'tos',
            'method': 'post'
        },
        'check': {
            'uri': 'check',
            'method': 'get'
        },
        'me': {
            'uri': 'user/me',
            'method': 'get'
        },
        "add_aws": {
            'uri': 'cloud/aws',
            'method': 'post'
        },
        "add_azure": {
            'uri': 'cloud/azure',
            'method': 'post'
        },
        "add_gcp": {
            'uri': 'cloud/gcp',
            'method': 'post'
        },
        "add_alibaba_cloud": {
            'uri': 'cloud/alibaba_cloud',
            'method': 'post'
        },
        "get_version": {
            'version': 'version',
            'method': 'get'
        },
        "status_aws": {
            'uri': 'cloud/status/aws',
            'method': 'post'
        },
        "status_azure": {
            'uri': 'cloud/status/azure',
            'method': 'post'
        },
        "status_gcp": {
            'uri': 'cloud/status/gcp',
            'method': 'post'
        },
        "status_alibaba_cloud": {
            'uri': 'cloud/status/alibaba_cloud',
            'method': 'post'
        }
    }

    def __init__(self, hostname, user, password, customer=None):
        """
        Init Function for Public API which will get credentials
        :param hostname:
        :param user:
        :param password:
        :param customer:
        """
        self.hostname = hostname
        self.user = user
        self.password = password
        self.customer = customer
        self.logger = logger

        # For redlock.io and myredlock.io prefix with https:// and for rest others http://
        if ('redlock.io' in hostname) or ('myredlock.com' in hostname) or ('prismacloud' in hostname):
            self.base_url = "https://" + hostname
        else:
            self.base_url = 'http://' + hostname

        self.session = requests.Session()
        self.session.auth = (self.user, self.password)
        self.session.headers.update({'Content-Type': 'application/json'})
        self.session.verify = False

    @auto_refresh
    def rest_send_request(self, uri, method, data=None, headers=None, params=None, timeout=120, reason=False,
                          **kwargs):
        """
        Function to send any type of request to a remote machine
        :param uri:
        :param method:
        :param data:
        :param headers:
        :param params:
        :param timeout:
        :param reason: Return Reason (Rest Return Code as text)
        :kwargs log_level: log level defined from function call
        :return:
        """
        url = urlparse.urljoin(self.base_url, uri)
        # Constructing logger with defined log level
        logger = self.logger.info
        if "log_level" in kwargs.keys():
            logger = eval("self.logger." + kwargs["log_level"])
        self.logger.debug("Making {} call to {}".format(method, url))
        if data:
            logger("Data: {}".format(data))
        if params:
            self.logger.debug("Params: {}".format(params))
        response = self.session.__getattribute__(method.lower())(url, data=data, headers=headers, params=params,
                                                                 timeout=timeout)
        logger("Response: {} ({})".format(response.status_code, str(response.reason).upper()))
        try:
            logger("Response Content: {}".format(response.content.decode("utf-8")))
            if response.status_code != requests.codes.ok and "x-redlock-status" in response.headers:
                logger(response.headers['x-redlock-status'])

            # if user has requested for reason, then we first check if there is something in 'x-redlock-status' and
            # return that, else we return request.reason back to the user (if requested)
            if reason:
                if 'x-redlock-status' in response.headers:
                    return response.status_code, response.content.decode("utf-8"), response.headers['x-redlock-status']
                else:
                    return response.status_code, response.content.decode("utf-8"), response.reason
            else:
                return response.status_code, response.content.decode("utf-8")
        except UnicodeDecodeError as err:
            self.logger.error("Having troubles parsing as utf-8 - ignoring for the moment this error: {}".format(err))
            if reason:
                return response.status_code, response.content, response.reason
            else:
                return response.status_code, response.content

    def rest_login(self):
        """
        Function to perform a rest login on remote machine and save token for further rest calls
        :return:
        """
        payload = {
            "username": self.user,
            "password": self.password
        }
        if self.customer:
            payload['customerName'] = self.customer
        url = urlparse.urljoin(self.base_url, PublicApi.URI['login']['uri'])
        response = self.session.post(url, json=payload)
        if response.status_code not in [requests.codes.OK, requests.codes.CREATED]:
            return None
        rest_response = response.json()
        if 'token' not in list(rest_response.keys()):
            self.logger.debug("Login requires customer Name")
            if 'customerNames' in list(rest_response.keys()):
                for customer in rest_response['customerNames']:
                    if 'redlock' in customer['customerName'].lower():
                        payload["customerName"] = customer["customerName"]
                        self.logger.debug(payload)
                        response = self.session.post(url=urlparse.urljoin(self.base_url, PublicApi.URI['login']['uri']),
                                                     data=json.dumps(payload))
                        if response.status_code not in [requests.codes.OK, requests.codes.CREATED]:
                            self.logger.error("Login unsuccessful")
                            return None
                        self.session.headers.update({'x-redlock-auth': response.json()['token']})
        else:
            self.session.headers.update({'x-redlock-auth': response.json()['token']})
            return 1

        return None

    def extend(self):
        """
        Function to extend the token expiry or session. If session has expired, perform a new login and update tokens
        :return:
        """
        response = self.session.get(url=urlparse.urljoin(self.base_url, PublicApi.URI['extend']['uri']))
        # 401 - Logged-Out, Log Back In
        # 200 - All Good, nothing to do, just update tokens
        # Anything-Else: Cannot do anything, need to raise an exception !!
        if response.status_code == requests.codes.UNAUTHORIZED:  # 401 - Logged Out, Log Back in
            self.logger.info("Session logged-out, Logging Back in")
            self.rest_login()
            return True
        if response.status_code == requests.codes.OK:  # Working fine, just update the tokens
            self.session.headers.update({'x-redlock-auth': response.json()['token']})
            return True
        self.logger.error(
            "ERROR !! Return Code '{}', Return Content '{}' !!".format(response.status_code, response.content))
        return False

    def version(self):
        response = self.rest_send_request(
            uri=PublicApi.URI['get_version']['version'],
            method=PublicApi.URI['get_version']['method']
        )

        return response[1]

    def accept_tos(self, include_username=True):
        """
        Function to accept terms of service agreement
        :return:
        """
        payload = {
            "password": self.password
        }
        if self.customer:
            payload['customerName'] = self.customer
        if include_username:
            payload['username'] = self.user

        response = self.session.post(url=urlparse.urljoin(self.base_url, PublicApi.URI['tos']['uri']),
                                     json=payload)
        if response.status_code not in [requests.codes.OK, requests.codes.CREATED]:
            return False

        return True

    def rest_send_request_with_token(self, uri, method, token, data=None, params=None, timeout=120):
        url = urlparse.urljoin(self.base_url, uri)
        print(url)
        headers = {'Authorization': token, 'Content-Type': 'application/json'}
        print(headers)
        try:
            response = requests.request(method, url, headers=headers, data=data, params=params,
                                        timeout=timeout)
        except requests.exceptions.RequestException as e:
            logger.info("Request failed. Error: {}".format(e))
        return response.status_code, response.text


if __name__ == "__main__":
    api = PublicApi(hostname="api14.qa.prismacloud.io", user="", password="")
    response = api.rest_login()
    print(response)
