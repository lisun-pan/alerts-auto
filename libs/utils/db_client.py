# import psycopg2.extras
# from sshtunnel import SSHTunnelForwarder
# import socket
# import os
#
# from libs.utils.logger import get_logger
# from sqlalchemy import create_engine
#
# logger = get_logger(os.path.basename(__file__).replace(".py", ""))
#
#
# def get_free_tcp_port():
#     """
#     Function to fetch free TCP Port for local bind address
#     :return:
#     """
#     tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     tcp.bind(('', 0))
#     address, port = tcp.getsockname()
#     tcp.close()
#     return int(port)
#
#
# class SSHTunnel(object):
#     _instance = None
#
#     def __new__(cls, *args, **kwargs):
#         """
#         Make an SSH Tunnel
#         :param args:
#         :param kwargs:
#         :return:
#         """
#         if not cls._instance:
#             cls._instance = super(SSHTunnel, cls).__new__(cls, *args, **kwargs)
#         return cls._instance
#
#     def __init__(self, ssh_host, ssh_port, ssh_private_key, ssh_user, remote_host, remote_port):
#         """
#         Init function to connect to SSH Tunnel and start the connection
#         :param ssh_host:
#         :param ssh_port:
#         :param ssh_private_key:
#         :param ssh_user:
#         :param remote_host:
#         :param remote_port:
#         """
#         self.logger = logger
#         self.ssh_host = ssh_host
#         self.ssh_port = ssh_port
#         self.ssh_private_key = ssh_private_key
#         self.ssh_user = ssh_user
#         self.remote_host = remote_host
#         self.remote_port = remote_port
#
#         self.ssh_tunnel = SSHTunnelForwarder(
#             (self.ssh_host, self.ssh_port),
#             ssh_username=self.ssh_user,
#             ssh_private_key=self.ssh_private_key,
#             remote_bind_address=(self.remote_host, self.remote_port),
#             local_bind_address=('localhost', get_free_tcp_port())
#         )
#         self.ssh_tunnel.start()
#         self.logger.info("Singleton Tunnel started")
#
#     @property
#     def local_bind_host(self):
#         """
#         Bind the Local Host
#         :return:
#         """
#         return self.ssh_tunnel.local_bind_host
#
#     @property
#     def local_bind_port(self):
#         """
#         Bind the Local Port
#         :return:
#         """
#         return self.ssh_tunnel.local_bind_port
#
#     def __del__(self):
#         self.ssh_tunnel.close()
#
#
# class DBConnect:
#
#     def __init__(self, database, user, password, host, port=5432, schema=None, **kwargs):
#         """
#         Make a connection with db
#         :param database: Name of the database to connect to
#         :param user: DB user
#         :param password: DB password
#         :param host: DB host
#         :param port: DB port
#         :param schema: DB schema
#         :param kwargs: If SSH tunneling is needed
#             ssh_host: ssh tunnel host
#             ssh_port: ssh tunnel port
#             ssh_private_key: id_rsa private key file for the user
#             ssh_username: ssh username
#         """
#         # Maybe get all these from a pre-known json file and load
#         self.logger = logger
#         self.host = host
#         self.database = database
#         self.user = user
#         self.password = password
#         self.port = port
#         self.ssh_host = None
#         self.ssh_port = None
#         self.ssh_private_key = None
#         self.ssh_tunnel = None
#         self.conn_cursor = None
#         self.connection = None
#         self.engine = None
#         self.schema = schema
#         if 'ssh_host' in kwargs.keys():
#             self.logger.info("Needs SSH Tunnel")
#
#             self.ssh_port = kwargs.pop('ssh_port', 22)
#
#             # make sure we have 'ssh_host', 'ssh_private_key' and 'ssh_user' in kwargs
#             assert 'ssh_host' in kwargs.keys(), "ERROR: 'ssh_host' not found in kwargs"
#             self.ssh_host = kwargs['ssh_host']
#
#             assert 'ssh_private_key' in kwargs.keys(), "ERROR: 'ssh_private_key' not found in kwargs"
#             self.ssh_private_key = kwargs['ssh_private_key']
#
#             assert 'ssh_user' in kwargs.keys(), "ERROR: 'ssh_user' not found in kwargs"
#             self.ssh_user = kwargs['ssh_user']
#
#     def connect(self):
#         """
#         Perform db connect
#         :return:
#         """
#         # Make SSH tunnel if ssh_host is defined
#         if self.ssh_host:
#             self.logger.info("Initializing tunnel for {} {}".format(self.ssh_host, self.ssh_port))
#             self.ssh_tunnel = SSHTunnelForwarder(
#                 (self.ssh_host, self.ssh_port),
#                 ssh_username=self.ssh_user,
#                 ssh_pkey=self.ssh_private_key,
#                 remote_bind_address=(self.host, self.port),
#                 local_bind_address=('localhost', int(get_free_tcp_port()))
#             )
#             self.ssh_tunnel.start()
#             self.logger.info("SSH Tunnel started")
#         self.logger.info("Trying to connect to DB {} {} {}".format(self.host, self.port, self.database))
#         self.connection = psycopg2.connect(
#             database=self.database,
#             user=self.user,
#             password=self.password,
#             host=self.ssh_tunnel.local_bind_host if self.ssh_tunnel else self.host,
#             port=self.ssh_tunnel.local_bind_port if self.ssh_tunnel else self.port,
#             options='-c search_path=' + self.schema if self.schema else ''
#         )
#         self.connection.autocommit = True
#         self.logger.info("Database connection established")
#
#     def connect_sqlalchemy(self):
#         """
#         Perform db connect
#         :return:
#         """
#         # Make SSH tunnel if ssh_host is defined
#         if self.ssh_host:
#             ssh_tunnel = SSHTunnelForwarder(
#                     (self.ssh_host, self.ssh_port),
#                     ssh_username=self.ssh_user,
#                     ssh_private_key=self.ssh_private_key,
#                     remote_bind_address=(self.host, self.port),
#                     local_bind_address=('localhost', 5432)
#             )
#             ssh_tunnel.start()
#             self.logger.info("SSH Tunnel started {}".format(self.ssh_tunnel))
#         self.logger.info("Trying to connect to DB {} {} {}".format(self.host, self.port, self.database))
#         self.engine = create_engine("postgresql://runtime:{}@localhost/{}".format(self.password, self.database))
#
#         return self.engine
#
#     def __del__(self):
#         """
#         Close DB connection and stop ssh tunnel
#         :return:
#         """
#         if self.connection is not None:
#             self.connection.close()
#         if self.engine is not None:
#             self.engine.dispose()
#         # Close SSH Tunnel
#         if self.ssh_tunnel:
#             self.ssh_tunnel.stop()
#
#     @property
#     def cursor(self):
#         if not self.conn_cursor:
#             self.conn_cursor = self.connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
#         return self.conn_cursor
#
#     @staticmethod
#     def insert_query(row_dict, table_name):
#         insert_sql = "INSERT into {} (".format(table_name)
#         values = "values ("
#         for k, v in row_dict.items():
#             insert_sql += " {},".format(k)
#             if isinstance(v, str):
#                 values += " '{}',".format(v)
#             else:
#                 values += " {},".format(v)
#         final_sql = insert_sql[:-1] + ") " + values[:-1] + " )"
#         return final_sql
#
#
# if __name__ == "__main__":
#     conn = DBConnect(
#        host='qa14-ng-t1-sdb-0.c1gmeifrcila.us-east-2.rds.amazonaws.com',
#        port=5432,
#        user='runtime',
#        password='G6JRyT9Nj0BXTs8GbGN46vMsUxr4vOVi',
#        database="sdb_app14_qa",
#        ssh_host='3.130.19.50',
#        ssh_user='luliu',
#        ssh_private_key='/Users/luliu/.ssh/id_rsa')
#     conn.connect()
#     sql = "select * from tenant_3.account_type"
#     cursor = conn.cursor
#     cursor.execute(sql)
#     rows = cursor.fetchall()
#     print(rows)
#     del conn