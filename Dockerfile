FROM python:alpine3.7

RUN pip3 install --upgrade pip \
    && ln -sv /usr/bin/python3 /usr/bin/python
RUN mkdir /root/alerts-auto
ADD ./ /root/alerts-auto/
WORKDIR /root/alerts-auto
RUN pip install --upgrade pip
RUN apk add make
RUN apk add libffi-dev openssl-dev

RUN apk add --no-cache snappy g++ snappy-dev && \
    pip install --no-cache-dir --ignore-installed python-snappy
RUN pip install cffi
RUN pip install pynacl

RUN python3 -m pip install --quiet -r /root/alerts-auto/requirements.txt
#CMD ["py.test", "tests/journey1/test_apiToCassandra.py"]

#RUN  mkdir -p /root/allure-results/
#RUN chmod 777 /root

RUN chmod +x run.sh
#RUN chmod +x allure.sh
CMD ["./run.sh"]
#CMD ["./allure.sh"]

