.PHONY: help
help:
	@echo "make test  : doing the test locally"
	@echo "make dtest : doing the test by using container"
	@echo "make build : building the local image"
	@echo "make clean : clean the cache files"
	@echo "make style : doing style checking locally"

.PHONY: test
test:
	@echo "Testing....."
	./run.sh

.PHONY: test
dtest:
	@echo "Testing....."
	docker run --net="host" alertstest


.PHONY: build
build:
	@echo "Build docker image"
	docker build -t redlock/alerts-auto:latest .
	docker push redlock/alerts-auto:latest

.PHONY: clean
clean:
	@echo "clean the project"
	find . -iname ".pytest_cache" -o -iname "__pycache__" -o -iname "*.pyc" -o -iname "report" | grep -v .idea | xargs rm -rfv
.PHONY: style
style:
	@echo "running pylint........................."
	pylint tests/
