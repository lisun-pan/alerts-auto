#!/bin/sh

#cmd="py.test -ra tests/"
cmd="py.test --alluredir allure-results -ra tests/"
#cmd ="sl-python pytest --teststage 'Integration tests' --tokenfile tests/sltoken.txt --buildsessionid 67febccd-f5d6-4729-808e-3c8d10371f59 --cov-report /tmp/report-sl --labid Lab1"

if [ -n "$TAG" ]
then
  cmd="$cmd -m $TAG"
  echo "$cmd"
fi

$cmd

# Repo Name
REPO_NAME=alerts-auto

# Local Static Variables
BIT_BUCKET=bitbucket.org:dlpqa
REPO_LOCAL_PATH=/root/${REPO_NAME}

#cp /root/hello.sh /root/dlp-e2e-test/results/hello_new.sh

echo "************* ENVIRONMENT VARIABLES *************"
[[ ! -z "${TEST_SERVER}" ]] && echo "TEST_SERVER: ${TEST_SERVER}"
[[ ! -z "${USER_NAME}" ]] && echo "USER_NAME: ${USER_NAME}"
[[ ! -z "${CUSTOMER}" ]] && echo "CUSTOMER: ${CUSTOMER}"
[[ ! -z "${TEST_BED}" ]] && echo "TEST_BED: ${TEST_BED}"
[[ ! -z "${TAGS}" ]] && echo "TAGS: ${TAGS}"

[[ ! -z "${TEST_DIR}" ]] && echo "TEST_DIR: ${TEST_DIR}"
[[ ! -z "${TEST_FILE}" ]] && echo "TEST_FILE: ${TEST_FILE}"

[[ ! -z "${EXTRA_ARGS}" ]] && echo "EXTRA_ARGS: ${EXTRA_ARGS}"

[[ ! -z "${GIT_BRANCH}" ]] && echo "GIT_BRANCH: ${GIT_BRANCH}"

[[ ! -z "${CLOUD_ACCOUNT}" ]] && echo "CLOUD_ACCOUNT: ${CLOUD_ACCOUNT}"
[[ ! -z "${TEST_DATA_FILE}" ]] && echo "TEST_DATA_FILE: ${TEST_DATA_FILE}"

[[ ! -z "${TIMEOUT}" ]] && echo "TIMEOUT: ${TIMEOUT}"
[[ ! -z "${CONCURRENCY}" ]] && echo "CONCURRENCY: ${CONCURRENCY}"

[[ ! -z "${PYTHONDONTWRITEBYTECODE}" ]] && echo "PYTHONDONTWRITEBYTECODE: ${PYTHONDONTWRITEBYTECODE}"
[[ ! -z "${PYTHONHASHSEED}" ]] && echo "PYTHONHASHSEED: ${PYTHONHASHSEED}"

[[ ! -z "${PYCURL_SSL_LIBRARY}" ]] && echo "PYCURL_SSL_LIBRARY: ${PYCURL_SSL_LIBRARY}"
[[ ! -z "${LDFLAGS}" ]] && echo "LDFLAGS: ${LDFLAGS}"
[[ ! -z "${CPPFLAGS}" ]] && echo "CPPFLAGS: ${CPPFLAGS}"

[[ ! -z "${LOG_LEVEL}" ]] && echo "LOG_LEVEL: ${LOG_LEVEL}"
echo "************* ENVIRONMENT VARIABLES *************"


# ALLURE CHECK
ALLURE_DIR=${REPO_LOCAL_PATH}/allure-results/
echo "ALLURE_DIR: ${TEST_DIR}"
if [[ ! -d "${ALLURE_DIR}" ]]; then
    mkdir -p ${ALLURE_DIR}
fi

# Switch to repo folder for running py.test command
cd ${REPO_LOCAL_PATH}

# Print Extra Debug Information
#echo "Git-Branch: $(git branch 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')"
#echo "Git-Hash # : $(git log --pretty=format:'%h' -n 1)"

#cd tests/tenant-config/
ls -ltra

echo "Executing Test"

#base_command="py.test tests/tenant-config/test_temp.py"

#base_command="PYTHONHASHSEED=0 py.test -v --html Regression_Report.html --alluredir /root/dlp-e2e-test/allure-results/ tests/tenant-config/test_e2e_flow.py"

base_command="PYTHONHASHSEED=0 py.test -m PROD -v --html Regression_Report.html --alluredir /root/alerts-auto/allure-results/ tests"

echo ${base_command}

eval ${base_command}

#cp /root/hello.sh /root/dlp-e2e-test/results/hello_new.sh