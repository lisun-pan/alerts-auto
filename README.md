 Repo for alerts backend automation testing
 
 Working with different tags:
 http://doc.pytest.org/en/latest/example/markers.html
 
 Docker steps:
 1. build the docker: `make build`
 2. Use `docker run --net="host" alertstest` to run all the test cases or use  `docker run  -e TAG=FUNCTIONAL --net="host" alertstest` to specify the test cases;
 or run `docker run  -v /tmp/report/:/root/alerts-auto/report/ -e TAG=FUNCTIONAL --net="host" alertstest` to get the report in the host `/tmp/report`
 
 API test:
docker run -v ~/report/:/root/alerts-auto/report/ -e TAG=API -e API_REST=http://localhost:9081/   -e API_AGGREGATOR=http://localhost:9083/ --net="host" redlock/alerts-auto:latest